<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Komentar</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Apa itu komentar HTML ?</h4>
	<p class="p-content">Komentar didalam HTML adalah sebuah kode yang digunakan untuk memberikan informasi tambahan untuk seorang web design atau web programing dan bukan untuk diberikan kepada user atau client.<br>
	</p>

	<h4 class="sub-heading">Kenapa kita perlu komentar ?</h4>
	<p class="p-content">Sebetulnya walaupun kita tidak menggunakan kometar itu tidak masalah, tapi problemnya misalkan kita buat sebuah program pada hari ini terus 5 tahun atau 10 tahun lagi kita buka lagi program yang kita buat hari ini, apa bila kita tidak menggunakan komentar mungki source code yang kita tulis sekarang mungkin bisa saja membuat kita bingung sendiri. <br>Dengan komentar juga akan memudahkan kita dalam menemukan bagian-bagian dari program kita, apa bila program yang kita buat adalah program yang cukup kompleks. <br>Kenapa ? <br>Karena komentar yang kita buat itu tidak akan dieksekusi dan tidak akan ditampilkan ke web browser.</p>
	
	<h4 class="sub-heading">Bagaimana cara menuliskan komentar pada HTML ?</h4>
	<p class="p-content">
	Untuk membuat komentar di HTML, kita menggunakan awalan <span class="p-bold">&lt;!-- </span> dan ditutup dengan <span class="p-bold">--&gt;</span> <br> Contoh :</p>

	<div class="contoh-dokumen">
		<h3>&lt;!-- Ini adalah komentar --&gt;</h3>
		<h3>&lt;!-- Komentar tidak akan ditampilkan ke user --&gt;</h3>
		<h3>&lt;!-- Komentar tidak akan ditampilkan di web browser --&gt;</h3>
	</div>
	<p class="p-content">
		Keterangan :
		<ul id="ul-content">
			<li><span class="p-bold">&lt;!-- </span> merupakan kata awalan untuk membuat komentar</li>
			<li>Sedangkan <span class="p-bold">--&gt;</span> merupakan penutup komentar.</li>
			<li>Dan semua isinya di tempatkan didalam / ditengah-tengah <span class="p-bold">&lt;!-- </span>disini <span class="p-bold">--&gt;</span></li>
		</ul>
	</p>

	<br>
	<a href="index.php?html=atribut" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=paragraf" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
