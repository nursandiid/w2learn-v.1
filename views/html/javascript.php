<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML JavaScript</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Apa itu HTMl JavaScipt ?</h4>
	<p class="p-content">Sebetulnya bukan namanya HTML JavaScript tetapi tag script untuk untuk menyimpan kode JavaScript. <br>
	Sekilas kita ketahui mengetahui mengenai JavaScript, tetapi kita tidak akan bahas detail pada tutorial ini melainkan kita hanya lihat beberapa contoh yang bisa di lakukan oleh JavaScript. <br>
	Tetapi sebelum kita melihat contoh atau demo yang bisa dilakukan JavaScript, kita haru tahu dulu bagaimana cara menggunakan JavaScript.
	</p>

	<h4 class="sub-heading">Bagaimana menggunakan JavaScipt ?</h4>
	<p class="p-content">Untuk menggunakan JavaScript kita perlu tag HTML yang namanya <span class="p-bold">&lt;script&gt;</span>. <br>
	Kemudian semua kode javaScriptnya itu ditempatkan di antara tag <span class="p-bold">&lt;script&gt;&lt;/script&gt;</span>. <br>
	</p>

	<h4 class="sub-heading">Contoh :</h4>
	<h4 class="sub-heading">JavaScript dapat merubah isi elemen HTML.</h4>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			<span class="nbsp">&lt;meta charset="UTF-8"&gt;</span><br>
			<span class="nbsp">&lt;title&gt;</span></span>Belajar Break Line<span class="properti">&lt;/title&gt;<br>
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>

		<span class="nbsp properti">&lt;p&gt;</span>Ini adalah paragraf 1<span class="properti">&lt;/p&gt;</span><br>
		<span class="nbsp properti">&lt;p&gt;</span>Ini adalah paragraf 2<span class="properti">&lt;/p&gt;</span><br>
		
		<br><br>
		<span class="nbsp properti">&lt;script&gt;</span><br>
		<span style="margin-left: 60px;">
		var paragraf1 = document.getElementsByTagName('p')[0];</span><br>
		<span style="margin-left: 60px;">paragraf1.innerHTML ="paragraf1 sudah saya ganti dengan JavaScript";</span>
		<span class="properti nbsp">&lt;/script&gt;</span><br>
		<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
		</span>
	</div>
	<p class="p-content">Apabila script tersebut dijalankan maka isi dari tag <span class="p-bold">&lt;p&gt;Ini adalah paragraf 1&lt;/p&gt;</span> akan berubah menjadi <span class="p-bold">"paragraf1 sudah saya ganti dengan JavaScript"</span> 
	<div class="contoh-dokumen">
		<p>paragraf1 sudah saya ganti dengan JavaScript</p>
		<p>Ini adalah paragraf 2</p>
	</div>
	
	<h4 class="-sub-heading">Kenapa isi paragraf 1 berubah ? </h4>
	<p class="p-content">
	Jawabannya karena isi dari tag <span class="p-bold">&lt;p&gt;</span> sudah dimanipulasi oleh JavaScript. <br> Walaupun kita belum belajar JavaScript yang penting kita tahu dulu tag untuk menyimpan kode script, sebetulnya yang bisa disimpan pada tag <span class="p-bold">&lt;script&gt;</span> itu banyak bukan hanya JavaScript ada VBScript, jScript dan sebagainya. <br>Banyak sekali yang bisa dilakukan JavaScript, kita akan belajar JavaScript pada tutorial-tutorial selanjutnya.</p>

	<br>
	<a href="index.php?html=breakLine" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=warna" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->