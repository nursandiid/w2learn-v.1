<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Daftar Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Daftar pada HTML</h4>
	<p class="p-content">Pada tutorial sebelumnya kita hanya membahas mengenai list ordered atau list terurut, tutorial ini kita membahas mengenai list yang tidka terurut serta definition list.</p>

	<h4 class="sub-heading">List-Unordered List</h4>
	<p class="p-content">Digunakan untuk membuat sebuah daftar dimana tiap bagiannya memiliki nomor yang tidak terurut / dengan menggunakan simbol. <br> Contoh :</p>
	
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Pelajaran sekolah<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;ul&gt;</span> <br>
			<span class="properti nbsp">&lt;li&gt;</span>Matematika<span class="properti">&lt;/li&gt;</span><br>
			<span class="properti nbsp">&lt;li&gt;</span>Sejarah Indonesia<span class="properti">&lt;/li&gt;</span><br>
			<span class="properti nbsp">&lt;li&gt;</span>Bahasa Indonesia<span class="properti">&lt;/li&gt;</span><br>
		<span class="properti">&lt;/ul&gt;</span>
	</div>

	<p class="p-content">Outputnya :</p>
	<div class="contoh-dokumen">
		<h4>Pelajara sekolah</h4>
		<ul>
			<li>Matematika</li>
			<li>Sejarah Indonesia</li>
			<li>Bahasa Indonesia</li>
		</ul>
	</div>

	<h4 class="sub-heading">Type lain :</h4>
	<ul id="ul-content">
		<li>disc (bulat-bulat hitam) default</li>
		<li>square (kotak hitam)</li>
		<li>circle (bulat putih / agak berlubang)</li>
	</ul>
	<p class="p-content">Untuk menggunakan type diatas penggunaanya sama seperti <span class="p-bold">&lt;ol&gt;</span>.</p>

	<h4 class="sub-heading">List-Definition List</h4>
	<p class="p-content">List yang terakhir ini agak berbeda dengan 2 list sebelumnya, List ini digunakan untuk membuat daftar dimana tiap daftar tersebut memiliki penjelasan atau istilahnya (sub‐bagian). <br> Strukturnya :</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;dl&gt;</span> <br>
			<span class="properti nbsp">&lt;dt&gt;</span>Term 1<span class="properti">&lt;/dt&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Deskripsi 1<span class="properti">&lt;/dd&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Deskripsi 2<span class="properti">&lt;/dd&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Deskripsi 3<span class="properti">&lt;/dd&gt;</span><br>
				<span style="margin-left: 60px;" class="p-bold">...</span><br>
			<span class="properti nbsp">&lt;dt&gt;</span>Term 2<span class="properti">&lt;/dt&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Deskripsi 1<span class="properti">&lt;/dd&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Deskripsi 2<span class="properti">&lt;/dd&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Deskripsi 3<span class="properti">&lt;/dd&gt;</span><br>
				<span style="margin-left: 60px;" class="p-bold">...</span><br>
		<span class="properti">&lt;/dl&gt;</span>
	</div>

	<h4 class="sub-heading">Contoh penggunaan :</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;dl&gt;</span> <br>
			<span class="properti nbsp">&lt;dt&gt;</span>SMKP Ciwaringin<span class="properti">&lt;/dt&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Rekayasa Perangkat Lunak<span class="properti">&lt;/dd&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Teknik Komputer Jaringan<span class="properti">&lt;/dd&gt;</span><br>
				<span class="properti" style="margin-left: 60px;">&lt;dd&gt;</span>Perbankan Syariah<span class="properti">&lt;/dd&gt;</span><br>
		<span class="properti">&lt;/dl&gt;</span>
	</div>

	<p class="p-content">Outputnya :</p>
	<div class="contoh-dokumen">
		<dl>
			<dt>SMKP Ciwaringin</dt>
			<dd>Rekayasa Perangkat Lunak</dd>
			<dd>Teknik Komputer Jaringan</dd>
			<dd>Perbankan Syariah</dd>
		</dl>
	</div>
	
	<p class="p-contoh">Apabila teman-teman perhatikan definition list akan otomatis menambahkan indentasi. <br> List diatas merupakan macam-macam list yang ada pada HTML, dan beberapa contoh diatas merupakan beberapa contohnya saja, ada banyak sekali yang bisa dilakukan menggunakan list.</p>


	<br>
	<a href="index.php?html=daftar" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=tabel" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->