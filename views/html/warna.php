<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Warna</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Warna pada HTML</h4>
	<p class="p-content">Warna merupakan hal yang cukup penting dalam belajar pemrograman web, bukan cuman pemrograman web, dikehidupan nyata juga yang namanya warna itu membuat seseorang senang. <br><br>
	Begitu juga pada halaman web kita, dengan warna yang cocok yaitu warna pilihan kita, akan membuat seorang user atau pengunjung website nyaman berada di website kita. <br>
	HTML telah menediakan beberapa warna yang bisa kita gunakan diantaranya :</p>
	<ul id="ul-content">
		<li>Solid</li>
		<li>RGB (Red, Green, Blue)</li>
		<li>HEX / hexadesimal</li>
		<li>HSL</li>
		<li>HSLA</li>
		<li>RGB</li>
		<li>RGBA</li>
	</ul>

	<h4 class="sub-heading">Warna Solid</h4>
	<p class="p-content">Warna solid merupakan warna yang ada di kehidupan kita. <br>Contoh :
	</p>
	<ul id="ul-content">
		<li>blue, lightblue, mediumblue</li>
		<li>green, lightgreen</li>
		<li>pink, salmon</li>
		<li>yellow</li>
		<li>purple</li>
		<li>aqua</li>
		<li>silver</li>
		<li>black</li>
		<li>white</li>
		<li>red</li>
		<li>dll...</li>
	</ul>
	<p class="p-content">teman-teman juga bisa kunjungi website warna berikut :</p>
	<ul id="ul-content">
		<li><a href="http://www.colourlovers.com/" target="_blank">Colour Lovers</a></li>
		<li><a href="https://color.adobe.com/explore/newest/" target="_blank">Adobe Color CC</a></li>
		<li><a href="https://coolors.co/browser/latest/1" target="_blank">Coolors</a></li>
		<li><a href="http://colorhunt.co/" target="_blank">Color Hunt</a></li>
	</ul>

	<h4 class="sub-heading">Warna RGB (Red, Green, Blue)</h4>
	<p class="p-content">Warna HTML selanjutnya adalah warna RGB atau merupakan kependekan dari Red, Green, BLue, pasti teman-teman sudah ada gambaran mengenai warna ini, kalau lihat dari namanya RGB, merupakan warna gabungan dari ketiga warna yaitu Red, Green, dan Blue. <br>Contoh :</p>
	<ul id="ul-content">
		<li>rgb(0,0,0)</li>
		<li>rgb(255,200,100)</li>
		<li>rgb(45,45,45)</li>
	</ul>
	<p class="p-content">Warna-warna diatas merupakan gabungan dari ketiga warna berbeda sehingga menjadi warna satu yang bagus dan indah.</p>

	<h4 class="sub-heading">Warna RGBA (Red, Green, Blue, Alpha)</h4>
	<p class="p-content">Warna HTML selanjutnya adalah warna RGBA atau merupakan kependekan dari Red, Green, BLue, dan Alpha. <br>
	RGBA sendiri hampir sama dengan RGB, perbedaanya adalah kalau RGBA itu ada a-nya yang artinya alpaha atau istilahnya transparansi.<br>
	Jadi dengan RGBA kita dapat atur transparansi dari warna kita itu mau seberapa transparan, ukuran transparannya itu paling besar itu 1 artinya solid, dan paling kecil atau pling transparan itu 0 artinya tidak kelihatan
	<br>Contoh :</p>
	<ul id="ul-content">
		<li>rgb(0,0,0, 0)</li>
		<li>rgb(255,200,100, .5)</li>
		<li>rgb(45,45,45, .2)</li>
	</ul>
	<p class="p-content">Teman-teman juga harus tahu tanda koma(,) pada HTML itu digantikan dengan tanda titik(.)</p>

	<h4 class="sub-heading">Warna HSL dan HSLA</h4>
	<p class="p-content">Warna selanjutnya adalah HSL dan HSLA merupakan kependekan dari Hue Saturation Lightness begitu juga HSLA ada kata a-nya yang artinya alpha, kedua warna ini hampir mirip-mirip dengan GRB dan RGBA yang sudah kita bahas diatas. <br> Contoh :</p>
	<ul id="ul-content">
		<li>hsl(8, 50%, 30%)</li>
		<li>hsl(8, 50%, 30%, .3)</li>
	</ul>

	<h4 class="sub-heading">Warna HEX / hexadesimal</h4>
	<p class="p-content">Warna yang terakhir adalah warna HEX tau hexadesimal marupakan warna yang dimulai dari 0 sampai f, 0 artinya hitam sedangkan f putih. <br>
	Jadi warna hexadesimal adalah warna antara 0 sampai f, kemudian warna hexadesimal itu ada 6 digit angka. kemudian diawali juga dengan tanda hash(#).<br>Contoh :</p>
	<ul id="ul-content">
		<li>#fff</li>
		<li>#000</li>
		<li>#f9f9f9</li>
	</ul>
	<p class="p-content">Warna-warna diatas merupakan warna-warna yang ada pada HTML, kami mita maaf kalau tidak sertakan contoh warnanya sehingga teman-teman bisa memilih sendiri, mungkin untuk kumpulan kode-kode warna kita akan bahas pada tutorial CSS.</p>

	<br>
	<a href="index.php?html=javascript" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=css" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->