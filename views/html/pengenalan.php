<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Pengenalan HTML</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Apa itu HTML ?</h4>
	<p class="p-content">HTML merupakan kependekan dari Hyper Text Markup Language adalah sebuah bahasa markup yang digunakan untuk membuat sebuah halaman web dan menampilkan berbagai informasi di dalam sebuah browser atau web browser.</p>

	<p class="p-content">Jadi HTML sendiri itu bukan merupakan bahasa pemrograman tetapi merupakan bahasa markup.</p>

	<p class="p-content">Seperti yang teman-teman tahu kan yang namanya bahasa pemrograman itu kan punya yang namanya variabel, tipe data, pengkondisian, perulangan, dan lain sebagainya. </p>

	<p class="p-content">Sedangkan HTML tidak, terkadang banyak sekali teman-teman yang keliru menyebut HTML itu adalah bahasa pemrograman padahal HTML itu bukan bahasa pemrograman tetapi bahasa markup.</p>

	<h4 class="sub-heading">Kenapa HTML disebut bahasa markup / Markup Language ?... </h4>

	<p class="p-content">Karena didalamnya itu terdapat serangkaian markup yang natinya kita sebut dengan tag, tag sendiri befungsi untuk memberi tahu kepada browser bagaimana sebuah konten akan ditampilkan.</p>

	<p class="p-content">Pasti teman-teman sering lihatlah sebuah halaman web yang apabila di inspect akan muncul sekumpulan kode-kode HTML.</p>

	<p class="p-content">Nah itu merupakan bahasa HTML / Markup Languange yang digunakan untuk membuat sebuah halaman web.</p>

	<p class="p-content">Dengan kita menguasai bahasa HTML nantinya kita akan bisa membuat sebuah halaman web sendiri yang bagus dan sesuai keinginan tentunya.</p>

	<p class="p-content">Di tutorial ini juga nantinya teman-teman akan diberikan pemahaman mengenai bahasa HTML. Sehingga teman-teman juga bisa membuat sendiri halaman webnya.</p>

	<a href="" class="btn btn-default btn-prev disabled">Sebelumnya</a>
	<a href="index.php?html=sejarah" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->