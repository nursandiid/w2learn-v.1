<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Link</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Link pada HTML</h4>
	<h4 class="sub-heading">Contoh hyperlink :</h4>
	<ul id="ul-content">
		<li><a href="">Google.com</a></li>
		<li><a href="">halaman 2</a></li>
	</ul>

	<p class="p-content">Link atau istilahnya hyperlink merupakan teknologi yang cukup ampuh, karena dengan menggunakan hyperlink kita dapat pindah dari 1 halaman ke halaman lain maupun dari satu website ke website lain. <br>
	Link atau hyperlink adalah sebuah koneksi dari sumber web ke sumber lain itu menurut spesifikasinya, dengan kata lain apabila tidak ada hyperlink maka tidak akan ada juga yang disebut dengan web. <br>
	Komponen hyperlink juga merupakan komponen yang sangat penting dalam web, link didalam HTML dibuat dengan tag <span class="p-bold">&lt;a&gt;&lt;/a&gt;</span> atau singkatan dari anchor yang artinya jangkar.</p>
	
	<h4 class="sub-heading">Kenapa disebut jangkar ?</h4>
	<p class="p-content">Karena analoginya yang namanya jangkarkan itu tehubung dengan sebuah kapal, begitu juga dengan link, link juga pasti tehubung dengan sebuah sumber.</p>

	<p class="p-content">Kemudian untuk membuat sebuah link kita juga perlu sebuah atribut yang namanay <span class="p-bold">href</span> jadi href ini merupakan atirbut yang akan memindahkan halaman kita ke website lain atau sumber lain.</p>
	
	<h4 class="sub-heading">Contoh penggunaan hyperlink :</h4>
	<p class="p-content">Misal saya punya sebuah paragraf yang isinya "klik disini untuk informasi lebih kanjut".</p>
	<div class="contoh-dokumen">
		<span class="nbsp properti">&lt;p&gt;</span>Klik disini untuk informasi lebih lanjut<span class="properti">&lt;/p&gt;</span>
	</div>
	<p class="p-content">Nantinya kata <span class="p-bold">disini</span> akan saya jadikan hyperlink.</p>
	<div class="contoh-dokumen">
		<span class="nbsp properti">&lt;p&gt;</span>Klik <span class="properti">&lt;a href =""&gt;</span>disini<span class="properti">&lt;/a&gt;</span> untuk informasi lebih lanjut<span class="properti">&lt;/p&gt;</span>
	</div>

	<p class="p-content">Apabila saya jalankan maka kata <span class="p-bold">disini</span> akan berubah warnanya menjadi biru dan ada garis bawah, kemudian pada saat cursor kita menyntuh kata <span class="p-bold">disini</span> cursornya berubah menjadi tangan.</p>
	<div class="contoh-dokumen">
		<p>Klik <a href="" style="text-decoration: underline; color: blue;">disini</a> untuk informasi lebih lanjut</p>
	</div>

	<p class="p-content">Defaultnya link apabila belum pernah kita kunjungi itu warnanya biru dan apabila sudah pernah kita kunjungi itu warnanya berubah menjadi ungu. <br>
	Teman-teman juga harus tahu cara penulisan hyperlink diatas sebetulnya sudah benar kita sudah tambahkan atribut <span class="p-bold">href</span> pada tag <span class="p-bold">a</span> tetapi kita <span class="p-bold">href</span> -nya masih kosong itu defaultnya halaman ahanya akan direload atau istilahnya reload ulang, kita tidak pergi ke sumber manapun atau halaman manapun karena kita belum tulis sumbernya, agar kita bisa pindah ke halaman lain atau website lain misalkan google.com, itu kita perlu tuliskan sumbernya berarti kalau google.com yah harus http://google.com, baru apa bila kita klik disini kita akan pindah ke google.com. <br>Kita cobah :</p>

	<h4 class="sub-heading">Contoh eksternal hyperlink :</h4>
	<div class="contoh-dokumen">
		<span class="nbsp properti">&lt;p&gt;</span>Klik <span class="properti">&lt;a href ="</span>http://google.com<span class="properti">"&gt;</span>disini<span class="properti">&lt;/a&gt;</span> untuk informasi lebih lanjut<span class="properti">&lt;/p&gt;</span>
	</div>
	<p class="p-content">Apabila dijalankan :</p>
	<div class="contoh-dokumen">
		<p>Klik <a href="http://google.com" style="text-decoration: underline; color: blue;">disini</a> untuk informasi lebih lanjut</p>
	</div>
	<p class="p-content">Coba teman-teman klik kata <span class="p-bold">disini</span> pasti halamannya akan pindah ke google.com. <br>
	Contoh diatas merupakan eksternal hyperlink</p>

	<h4 class="sub-heading">Hyperlink terbagi menjadi 2 :</h4>
	<ul id="ul-content">
		<li>Internal hyperlink</li>
		<li>Eksternal hyperlink</li>
	</ul>

	<h4 class="sub-heading">Internal hyperlink</h4>
	<p class="p-content">Internal hyperlink merupakan cara kita untuk dapat pindah halaman dalam satu domain yang sama, domainnya bisa domain host(web hosting) bisa juga domain local atau istilahnya komputer kita. <br>
	Jadi dengan internal hyperlink kita bisa pindah halaman misalkan dari folder latihan1 ke folder latihan2.</p>

	<p class="p-content">Coba teman-teman buat di partisi D / localdisk D 1 folder yang namanya belajar, ingat juga teman-teman biasakan untuk memberi nama file html kita atau folder html kita dengan huruf kecil dan tanpa menggunakan spasi, untuk sementara ikutin saja nanti juga teman-teman akan lebih mudah dalam pemanggilan file atau pindah ke file lain dengan nama yang mudah diingat.</p>

	<p class="p-content">Buat 1 file di dalam folder belajar dengan nama latihan1.html yang isinya sebagai barikut.</p>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			<span class="nbsp">&lt;meta charset="UTF-8"&gt;</span><br>
			<span class="nbsp">&lt;title&gt;</span></span>Belajar Hyperlink / latihan1<span class="properti">&lt;/title&gt;<br></span>
		<span class="properti">
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>

		<span class="properti nbsp">&lt;a href ="</span>latihan2.html<span class="properti">"&gt;</span>latihan 2<span class="properti">&lt;/a&gt;</span><br>
		
		<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
		</span>
	</div>

	<p class="p-content">Selanjutanya kita buat 1 file lagi di folder yang sama dengan nama latihan2.html yang isinya :</p>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			<span class="nbsp">&lt;meta charset="UTF-8"&gt;</span><br>
			<span class="nbsp">&lt;title&gt;</span></span>Belajar Hyperlink / latihan2<span class="properti">&lt;/title&gt;<br></span>
		<span class="properti">
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>

		<span class="properti nbsp">&lt;h1&gt;</span>Selamat datang di latihan 2<span class="properti">&lt;/h1&gt;</span><br>
		
		<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
		</span>
	</div>

	<p class="p-content">Apabila dijalankan script diatas maka akan tampil :</p>
	<div class="contoh-dokumen">
		<a href="">latihan 2</a>
	</div>

	<p class="p-content">Dan apabila tulisan latihan2 di klik maka akan pindah ke halaman latihan 2 :</p>
	<div class="contoh-dokumen">
		<h1>Selamat datang di latihan 2</h1>
	</div>

	<p class="p-content">Teman-teman juga harus perhatikan untuk link internal kita tidak perlu tuliskan http atau istilahnya protocolnya kita cukup tuliskan nama filenya saja kalau berada pada folder yang lain tinggal kita tulis nama foldernya saja, berbeda dengan eksternal link karena eksternal link merupakan cara kita untuk pindah ke website lain atua sumber lain berarti kita haru spesifik juga nulisin alamatnya atau sumbernya. <br>Teman-teman juga bisa berkolaborasi sendiri dengan menggunakan hyperlink, misalkan ingin pindah dari latihan1 le latihan2, kemudian dari latihan2 ke latihan1 teman-teman bisa coba sendiri.</p>

	<br>
	<a href="index.php?html=css" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=gambar" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhit content -->