<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Gambar</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Gambar pada HTML</h4>
	<p class="p-content">Dengan menggunakan gambar akan membuat halaman web kita tampil menjadi lebih menarik. <br>
	Pada tutorial ini kita akan membahas mengenai gambar pada HTML, ada beberapa hal yang berkaitan dengan gambar misalkan :</p>
	<ul id="ul-content">
		<li>Tag yang digunakan untuk menampilkan gambar</li>
		<li>Gambar itu punya nama</li>
		<li>Gambar itu punya sumber</li>
		<li>Gambar itu punya deskripsi</li>
		<li>Gambar itu punya judul</li>
		<li>Gambar itu punya tinggi dan lebar</li>
		<li>dll...</li>
	</ul>
	
	<h4 class="sub-heading">Tag apa untuk menampilkan gambar ?</h4>
	<p class="p-content">Di HTML itu ada beberapa cara untuk menampilkan gambar diantaranya adalah dengan menggunakan tag <span class="p-bold">&lt;img&gt;</span>, walaupun apabila teman-teman sudah belajar CSS, CSS juga bisa memanggil gambar dengan properti <span class="p-bold">background-image: url(.....);</span> tetapi kita tidak akan bahas yang CSS-nya melainkan kita hanya akan bahas tag HTML nya saja. <br> Intinya tag HTML yang digunakan untuk menampilkan gambar adalah tag <span class="p-bold">&lt;img&gt;</span>.</p>

	<h4 class="sub-heading">Gambar itu punya nama</h4>
	<p class="p-content">Yah, gambar yang akan kita tampilkan di halaman web kita harus ada namanya, karena dalam proses pencarian tempat gambar kita akan cari namanya, usahakan namanya yang mudah di ingat.</p>

	<h4 class="sub-heading">Kenapa harus ada sumber ?</h4>
	<p class="p-content">Sumber itu cukup penting karena kita akan ada yang namanya proses penelusuran tempat letak gambar, misalkan kita menyimpannya dimana, di komputer kita atau di initernet, kita harus tuliskan sumbernya. <br> Nantinya ada atribut dari <span class="p-bold">&lt;img&gt;</span> yang namanya <span class="p-bold">src</span>. atau source yang artinya sumber.<br>
	<span class="p-bold">src</span> inilah yang akan mencari dimana gambar kita disimpan.</p>

	<h4 class="sub-heading">Kita harus punya deskipsi dari gambar kita</h4>
	<p class="p-content">Dengan deskripsi gambar kita akan mendapatkan keuntungan sepeti pada saat kita memanggil gambar terus gambarnya kita tidak ketemu, dengan deskripsi gambar maka gambarnya akan digantikan dengan nama dari gambar tersebut misalkan img1.jpg atau sebagainya. <br><br>
	Untuk membuat deskripsi pada gambar kita perlu atribu dari <span class="p-bold">&lt;img&gt;</span> yang namanya <span class="p-bold">alt</span>, nah alt inilah yang akan menggantikan gambar apabila gambarnya hilang atau tidak ketemu.</p>

	<h4 class="sub-heading">Kenapa harus ada judul ?</h4>
	<p class="p-content">Dengan judul yang ada pada gambar kita, kita akan tahu oh ini gambar ini, oh itu gambar itu, terkadang kita punya gambar yang sama, oleh karena itu dengan judul kita akan mudah mencari gambar yang ada pada website kita.  <br><br>
	Judul sendiri akan muncul pada saat gambarnya di hover atau disorot. selain dari pada itu tidak akan muncul, pasti teman-teman sering lihatlah pada saat nyentuh gambar di website mana kemudian ada kotak kecil puti yang tulisannya nama dari gambar tersebut. <br>
	Untuk membuat judul atau title pada gambar itu kita gunakan atribut dari <span class="p-bold">&lt;img&gt;</span> yang namanya <span class="p-bold">title</span>, Nah title inilah yang akan menjadi judul dari gambar kita.
	</p>

	<h4 class="sub-heading">Kenapa harus ada tingi dan lebar ?</h4>
	<p class="p-content">Terkadang gambar yang kita pakai itu terlalau besar, terkadang juga kita males resize gambarnya di photoshop ataupun corel draw, oleh karena itu kita bisa manfaatkan atribut yang dimiliki <span class="p-bold">&lt;img&gt;</span>. <br>
	Tag <span class="p-bold">&lt;img&gt;</span> sendiri punya atribut yang namanya <span class="p-bold">height</span> dan <span class="p-bold">width</span>, nah kedua atribut inilah yang tugasnya untuk merubah ukuran gambar, walaupun memang yang dirubah dari ukurannya gambar tersebut adalah ukuran pada saat tampil di halaman web, bukan ukuran gambar yang asli. <br>
	Nilai dari atirbut ini ada beberapa, yang paling sering digunakan adalah <span class="p-bold">px, %</span> dan walupun kita tidak tuliskan itu, misalkan cuman nilainya misal 40 atau 50 itu defaultnya adalah <span class="p-bold">px</span>. Teman-teman juga harus tahu tag <span class="p-bold">&lt;px&gt;</span> ini tidak memiliki tag penutup sama seperti <span class="p-bold">br, hr</span>.<br>
	Setelah kita ketahui beberapa hal diatas, bagaimana kalau kita langsung praktekan.
	</p>

	<p class="p-content">Saya asumsikan teman-teman sudah punya folder yang namanya belajarhtml di disk D, saya juga asumsikan teman-teman sudah punya sebuah gambar dengan nama img.jpeg di folder belajarhtml tersebut dengan file html yang namanya index.html. <br>
	Kurang lebih tanpilannya seperti ini: <br></p>

	<img src="public/img/bg.jpg" class="hello"> <br><br>

	<p class="p-content">jika sudah coba salin script dibawah ini kemudian copy-kan di file HTML kalian. </p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;img src="</span>img.jpeg
		<span class="properti">" height="</span>50
		<span class="properti">" alt="</span>img.jpeg
		<span class="properti">" title="</span>Mobil keren
		<span class="properti">"&gt;</span>
	</div>
	
	<p class="p-content">Apabila kita jalankan maka akan tampil :</p>
	<div class="contoh-dokumen">
		<img src="public/img/img.jpg" alt="img.jpg" height="50" title="Mobil keren">
	</div>

	<ul id="ul-content">
		<li>src nya ke img.jpeg</li>
		<li>title akan muncul apabila teman-teman hover gambarnya, teman-teman bisa coba sendiri</li>
		<li>height -nya saya kasih 50 artinya 50px</li>
		<li>alt akan muncul apabila gambar tidak ditemukan.</li>
	</ul>
	<h4 class="sub-heading">Bagaimana ketahui <span class="p-bold">alt</span></h4>
	<p class="p-content">Teman-teman pasti tahukan di folder yang sama kita punya gambar yang namany img.jpeg, kalau kita paggilnya seperti ini misal :</p>

	<div class="contoh-dokumen"><img src="public/img/img1.jpeg" alt="img1.jpeg"></div>

	<p class="p-content">Karena gambar tidak ditemukan maka gambar digantikan dengan <span class="p-bold">alt</span> dan gambarnya hanya akan digantikan digantikan dengan icon gambar. <br>Teman-teman juga harus tahu :
	</p>
	<ul id="ul-content">
		<li>Ekstensi gambar itu berbeda jpg, jpeg, gif, png, dlll.</li>
		<li>Huruf besar huruf kecil berpengaruh artinya img dengan huruf kecil itu berbeda dengan IMG menggunakan huruf besar.</li>
		<li>Hati-hati menempatkan tempat gambar, kebetulan kita menempatkannya ada pada folder yang sama, bagainamana kalau kita simpannya di folder sendiri misalnkan untuk gambar saya simpannya di folder <span class="p-bold">img</span>, kita akan butuh yang namanya tanda (/) untuk masuk ke folder tertentu kemudian (../) untuk keluar dari folder kita, misal kita berada di folder belajarhtml yang ada di Disk D, kan kita harus keluar terlebih dahulu baru panggil gambarnya keluarnya dengan (../folder apa).</li>
		<li>Dan sebagainya...</li>
	</ul>


	<br>
	<a href="index.php?html=link" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=daftar" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->