<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Elemen</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4>Apa itu Elemen HTML ?</h4>
	<p class="p-content">Element adalah isi dari tag yang berada diantara tag pembuka dan tag penutup, termasuk tag itu sendiri dan atribut yang dimilikinya. <br>Misalkan ada contoh :</p>
	<div class="contoh-dokumen">
		<h1>&lt;h1&gt;Ini adalah Heading&lt;/h1&gt;</h1>
	</div>
	<div class="p-content">
		Ini artinya : &lt;h1&gt;Ini adalah Heading&lt;/h1&gt; merupakan sebuah elemen. Elemen sendiri boleh memiliki atribut boleh juga tidak (optional).
	</div>

	<h4>Apa itu tag HTML ?</h4>
	<p class="p-content">Tag adalah suatu tannda untuk menandai elemen-elemen dalam suatu dokumen HTML dan Fungsinya sendiri adalah untuk memberikan instruksi atau untuk memberitahukan kepada browser bagaimana suatu objek di tampilkan berdasarkan Tag yang di gunakan.</p>
	<h4>Objeknya apa saja ?</h4>
	objek disini bisa berupa teks, video, audio dan gambar. </p>
	
	<h4>Tag-tagnya apa saja ?</h4>
	
	<h4>Sebagai Dokumen Informasi</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;html&gt;&lt;/html&gt;</span>
	</div>
	<p class="p-content">Tag HTML digunakan untuk memberikan informasi kepada browser bahwa ini adalah halaman HTML.</p>

	<h4>Sebagai Head</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;head&gt;&lt;/head&gt;</span>
	</div>
	<p class="p-content">Tag head merupakan struktur dasar dari dokumen html, digunakan untuk mendefinisikan bagian kepala dokumen yang berisi informasi tentang dokumen.</p>

	<h4>Sebagai Body</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;body&gt;&lt;/body&gt;</span>
	</div>
	<p class="p-content">Tag body adalah sebagai parent atau wadah dari semua elemen HMTL, walaupun ada beberapa elemen yang berada pada tag head.</p>

	<h4>Tag-tag yang berada dibagian <span class="p-bold">&lt;head&gt;</span></h4>

	<div class="contoh-dokumen">
		<p class="p-content">Page Title / Judul Halaman :<br> <span class="p-bold">Contoh: &lt;title&gt; &lt;/title&gt;</span></p>

		<p class="p-content">Scripting :<br> Adalah tempat menyimpan Client Side script yang disertakan (JavaScript, VB Script, JScript) <br><span class="p-bold">Contoh: &lt;script&gt; &lt;/script&gt;</span></p>
		
		<p class="p-content">Style / CSS :<br> Style dipergunakan untuk mengatur bagaimana sebuah halaman web dengan berbagai komponennya hendak ditampilkan ke dalam browser. <br><span class="p-bold">Contoh: &lt;style&gt; &lt;/style&gt;</span></p>

		<p class="p-content">Meta :<br> Meta tags, meta descriptions, dan meta keywords digunakan untuk mempermudah search engine dalam melakukan pencarian. <br><span class="p-bold">Contoh: &lt;meta&gt; &lt;/meta&gt;</span></p>
	</div>

	<h4>Tag-tag yang berada dibagian <span class="p-bold">&lt;body&gt;</span></h4>

	<div class="contoh-dokumen">
		<p class="p-content">Heading</p>
		<ul>
			<li><span class="p-bold">&lt;h1&gt;, &lt;h6&gt;, &lt;h3&gt;, &lt;h4&gt;, &lt;h5&gt;, &lt;h6&gt;</span></li>
		</ul>

		<p class="p-content">Paragraf</p>
		<ul>
			<li><span class="p-bold">&lt;P&gt;</span></li>
		</ul>

		<p class="p-content">Pendukung Teks</p>
		<ul>
			<li><span class="p-bold">&lt;br&gt;, &lt;hr&gt;, &lt;strong&gt;, &lt;em&gt;, ....</span></li>
		</ul>

		<p class="p-content">Hyperlink</p>
		<ul>
			<li><span class="p-bold">&lt;a&gt;</span></li>
		</ul>

		<p class="p-content">Gambar</p>
		<ul>
			<li><span class="p-bold">&lt;img&gt;</span></li>
		</ul>

		<p class="p-content">List (Bullets & Numberring)</p>
		<ul>
			<li><span class="p-bold">&lt;ul&gt;, &lt;ol&gt;, &lt;li&gt;, &lt;dl&gt;, &lt;dt&gt;, &lt;dd&gt;, ....</span></li>
		</ul>

		<p class="p-content">Tabel</p>
		<ul>
			<li><span class="p-bold">&lt;table&gt;, &lt;thead&gt;, &lt;tbody&gt;, ....</span></li>
		</ul>

		<p class="p-content">Form</p>
		<ul>
			<li><span class="p-bold">&lt;form&gt;, &lt;input&gt;, &lt;select&gt;, &lt;button&gt;, &lt;radio&gt;....</span></li>
		</ul>

		<p class="p-content">Script</p>
		<ul>
			<li><span class="p-bold">&lt;script&gt;</span></li>
		</ul>

		<p class="p-content">Grouping</p>
		<ul>
			<li><span class="p-bold">&lt;div&gt;, &lt;span&gt;</span></li>
		</ul>

		<p class="p-content">Komentar</p>
		<ul>
			<li><span class="p-bold">&lt;!-- ini adalah komentar --&gt;</span></li>
		</ul>

		<p class="p-content">Dll....</p>
	</div>

	<a href="index.php?html=dasar" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=atribut" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->