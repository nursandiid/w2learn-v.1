<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML CSS</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">CSS pada HTML</h4>
	<p class="p-content">Setelah sebelumnya kita mengetahui cara menyimpan kode JavaScript di halaman web kita, kali ini kita akan ketahui bagaimana menyimpan CSS pada halaman web kita. <br>Ada beberapa cara untuk menyisipkan kode CSS pada halaman web kita, namun saya akan beri tahu satu saja mungkin sisanya akan saya jelaskan pada tutorial CSS. </p>
	
	<h4 class="sub-heading">Contoh :</h4>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			<span class="nbsp">&lt;meta charset="UTF-8"&gt;</span><br>
			<span class="nbsp">&lt;title&gt;</span></span>Belajar CSS<span class="properti">&lt;/title&gt;<br>
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>

		<span class="nbsp properti">&lt;h1&gt;</span>Ini merupakan heading 1<span class="properti">&lt;/h1&gt;</span><br>
		<span class="nbsp properti">&lt;h1&gt;</span>Warnanya akan saya ruba menjadi biru<span class="properti">&lt;/h1&gt;</span><br>
		
		<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
		</span>
	</div>

	<p class="p-content">Misalkan ada contoh seperti dokumen diatas, kalau saya ingin merubah warna isi dari tag <span class="p-bold">&lt;h1&gt;</span> bagaimana, dengan menggunakan CSS kita akana sangat mudah untuk memanipulasi elemen HTML terutama yang berkaitan dengan layout atau style. <br>
	Dan kalau teman-teman jelih waktu kita belajar mengenai elemen HTML pasti teman-teman sudah tahu file CSS harus disimpan dimana, yah disimpannya di bagian <span class="p-bold">&lt;head&gt;</span>.</p>

	<p class="p-content">Untuk menyimpan file CSS / kode CSS pada web kita, kita perlu tag HTML yang namanya <span class="p-bold">&lt;style&gt;</span>, Nantinya kita menyimpan file CSS-nya diantara tag pembuka dan tag penutup, walaupun sebetulnya untuk menyimpan file CSS itu bukan cara yang tadi disebutkan saja melainkan ada beberapa cara lagi misalkan inline dan eksternal, cara yang disebutkan tadi merupakan cara internal. Artinya apa ? <br>Artinya semua file CSS kita nanti kita simpan bersamaan dengan file HTML yang kita buat.</p>

	<h4 class="sub-heading">Contoh penggunaan CSS :</h4>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			<span class="nbsp">&lt;meta charset="UTF-8"&gt;</span><br>
			<span class="nbsp">&lt;title&gt;</span></span>Belajar CSS<span class="properti">&lt;/title&gt;<br></span>
		<span class="nbsp properti">&lt;style&gt;</span><br>
		<span style="margin-left: 60px;" class="properti">h1{color: blue;}</span><br>
		<span class="properti nbsp">&lt;/style&gt;</span><br>
		
		<span class="properti">
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>

		<span class="nbsp properti">&lt;h1&gt;</span>Ini merupakan heading 1<span class="properti">&lt;/h1&gt;</span><br>
		<span class="nbsp properti">&lt;h1&gt;</span>Warnanya akan saya ruba menjadi biru<span class="properti">&lt;/h1&gt;</span><br>
		
		<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
		</span>
	</div>

	<p class="p-content">Apabila script diatas dijalankan maka warna isi dari tag <span class="p-bold">h1</span> aka berubah menjadi warna biru.</p>

	<div class="contoh-dokumen" style="color: blue;">
		<h1>Ini merupakan heading 1</h1>
		<h1>Warnananya akan saya rubah menjadi biru</h1>
	</div>

	<p class="p-content">Cukup mudah bukan kita memanipulasi style pada elemen HTML dengan menggunakan CSS, contoh lagi misal saya ingin merubah backround-nya jangan putih misal salmon.</p>

	<h4 class="sub-heading">Contoh merubah background dengan CSS</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;style&gt;</span><br>
		<span style="margin-left: 30px;" class="properti">
			h1{ 
			<br> 
			<span style="margin-left: 60px;">color: blue;</span><br>
			<span style="margin-left: 60px;">backround-color: salmon;</span><br>
			<span class="nbsp">}</span>
		</span><br>
		<span class="properti">&lt;/style&gt;</span><br>
	</div>

	<p class="p-content">Apa bila script diatas dijalankan maka akan berubah background dari <span class="p-bol">h1</span> -nya menjadi salmon.</p>
	<div class="contoh-dokumen">
		<h1 style="background-color: salmon; color: blue;">Ini merupakan heading 1</h1>
		<h1 style="background-color: salmon; color: blue;">Warnananya akan saya rubah menjadi biru</h1>
	</div>

	
	<p class="p-content">Contoh diatas merupakan beberapa kemudahan menggunakan CSS, kita akan belajar CSS lebih jauh lagi pada tutorial-tutorial berikutnya.</p>

	<br>
	<a href="index.php?html=warna" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=link" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->