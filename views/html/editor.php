<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Editor</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Apa itu HTML Editor ?</h4>
	<p class="p-content">HTML Editor merupakan sebuah aplikasi untuk menuliskan kode HTML atau lebih tepatnya lagi kode editor / teks editor, dengan alat tersebutlah kita dapat membuat halaman web sendiri. <br><br>
	Banyal sekali kode editor di luaran sana yang cukup bagus, malah sebetulnya kalau anda pengguna windows itu ada yang namanya notepad, begitu juga pengguna Max OSX maupun Linux, tiap-tiap sistem operasi itu memiliki teks editor bawaan dari sistem operasi tersebut walaupun hanya sebuah plain teks biasa. <br><br>
	Pada tutorial ini kita akan bahas beberapa teks editor yang nantinya akan kita gunakan untuk menuliskan kode HTML.
	</p>

	<h4 class="sub-heading">Macam-maca teks editor untuk menuliskan kode HTML</h4>
	<h4 class="sub-heading">Notepad++</h4>
	<p class="p-content">
		Notepad++ adala sebuah teks editor yang hampir mirip-mirip dengan notepad , aplikasi ini juga dapat di download secara gratis  atau open source. <br>

		Aplikasi ini memiliki kelebihan dibandingkan notepad bawaan windows, yang pertama gratis, aplikasi ini sangat ringan, memiliki fitur autocomplete, jika anda menulis kode - kode aplikasi ini dapat membedakan dengan warna pada syntaks apa saja yang anda tulis. <br>
		
		Selain itu, Notepad++ juga dapat ditambahkan berbagai plugin yang bisa semakin mempermudah pekerjaan programmer. Dan juga Notepad++ terdapat versi portabelnya.
	</p>

	<h4 class="sub-heading">Atom</h4>
	<p class="p-content">Atom sendiri adalah aplikasi teks editor yang sedang naik daun, aplikasi ini hampir mirip dengan sublime text.</p>
	<ul id="ul-content">
		<li>Dengan menggunakan teks editor atom kita bisa langsung terhubung dengan projek yang ada di github.</li>
		<li>Atom juga memiliki plugin yang cukup banyak</li>
		<li>Cara menginstal packacge yang mudah.</li>
	</ul>

	<h4 class="sub-heading">Sublime Text</h4>
	<p class="p-content">Sublime Text adalah sebuah aplikasi teks editor yang menurut saya cukup hebat, karena aplikasi ini memiliki fitur-fitur yang cukup banyak, dan memiliki plugin-plugin yang cukup keren yang sangat membantu sekali buat seorang web design maupun web programing. <br>

	Sublime Text juga mendukung banyak sekali banyak bahasa pemrograman, dan sampai sekarang sublime text sendiri sudah sampai versi ke-3.
	</p>

	<br>
	<a href="index.php?html=karakteristik" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=hello" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
