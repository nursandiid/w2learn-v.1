<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Atribut</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Apa itu atribut HTML ?</h4>
	<p class="p-content">Apabila kita berbicara mengenai atribut pada HTML itu sangat berkaitan sekali dengan tag dan elemen. setelah sebelumnya kita telah mengetahui mengenai elemen HTML, kali ini kita akan membahas mengenai atribut. <br>
	Atribut adalah informasi tambahan yang kita berikan pada tag HTML.</p>
	
	<h4 class="sub-heading">Untuk apa atribut ?</h4>
	<p class="p-content">
	Atribut sendiri berguna untuk memberikan informasi tambahan yang akan diberikan kepada tag dan elemen yang mengandung atribut tersebut. <br>Informasinya bisa berupa instruksi untuk warna dari text, besar huruf, dll. Setiap atribut memiliki pasangan nama dan nilai (value) yang ditulis dengan <span class="p-bold">name=”value”</span>. Value diapit tanda kutip, boleh menggunakan tanda kutip satu (<span class="p-bold">‘</span>) atau dua (<span class="p-bold">“</span>).</p>

	<h4>Tag, Atribut, dan Value</h4>
	<div class="contoh-dokumen">
		<h1 class="atribut">&lt;nama_tag nama_atribut ="value"&gt;</h1>
	</div>
	<p class="p-content">
		Keterangan :
		<ul id="ul-content">
			<li>nama_tag merupakan nama dari tag html misal h1, p, a, dll...</li>
			<li>atribut, nah atribut ini berguna sebagai pemberi informasi, misal h1 saya kasih warna green, nah cara itu bisa kita lakukan dengan atribut.</li>
			<li>value, semua atribut tidak akan berjalan kalau tidak ada value yang kita tentukan.</li>
		</ul></p>
	<p class="p-content">
		Ketiga hal tersebut merupakan bagian dari elemen HTML, jadi satu elemen HTML itu pastikan memiliki tag, nah tag tersebut boleh memiliki banyak atribut beserta dengan value / nilainya.
	</p>

	<p class="p-content">Contoh :</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;body bgcolor ="blue"&gt;</span>
	</div>

	<p class="p-content">
		Keterangan :
		<ul id="ul-content">
			<li>&lt;body bgcolor ="blue"&gt;</li>
			<li>body merupakan tag HTML, tag body sendiri memiliki atribut yang namanya bgcolor, bgcolor sendiri merupakan salah satu atribut dari body yang digunakan untuk memberiakan warna pada background.</li>
			<li>blue adalah value atau nilai yang dimilik oleh atribut</li>
		</ul>
	</p>

	<br>
	<a href="index.php?html=elemen" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=komentar" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
