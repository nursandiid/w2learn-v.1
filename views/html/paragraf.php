<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Paragraf</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Paragraf pada HTML</h4>
	<div class="p-content">Contoh paragraf :</div>
	<div class="contoh-dokumen">
		<p>Ini adalah sebuah paragraf</p>
		<p>Paragraf ini dibuat menggunakan tag <span class="p-bold">&lt;p&gt;</span></p>
		<p>Dengan menggunakan paragraf maka isi atau contentnya akan otomatis turun kebawah.</p>
	</div>
	<p class="p-content">Untuk membuat paragraf baru, membuat text berada dalam sebuah paragraf. Tag paragraf bisa memiliki penutup boleh juga tidak. <br></p>

	<p class="p-content">Untuk membuat paragraf kita menggunakan tag <span class="p-bold">&lt;p&gt;  &lt;/p&gt;</span> <br></p>	

	<p class="p-content">Misalkan ada contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
			&nbsp;&nbsp;&lt;title&gt;</span>Belajar paragraf<span class="properti">&lt;/title&gt;<br>
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>
		<span class="nbsp">Ini adalah paragraf 1</span><br>
		<span class="nbsp">Ini adalah paragraf 2</span><br>
		</span>
		<span class="properti">
		&lt;/body&gt;<br>
		&lt;/html&gt;<br>
		</span>
	</div>

	<p class="p-content">Apa bila kita jalankan script diatas maka outputnya menjadi :</p>
	<div class="contoh-dokumen">
		Ini adalah paragraf 1 Ini adalah paragraf 2
	</div>

	<p class="p-content">Sekarang pertanyannya, Kenapa paragraf 2 tidak turun ke bawah ?.... <br> Padahalkan jelas-jelas kita nulisnya dibawah paragraf 1. <br>Sebetulnya jawabanya cukup simpel, karena browser tidak tahu paragraf 1, paragraf 2, itu ditampilkan sebagai apa, heading kah, link, ataupun sebagai paragraf. <br> Browser tidak tahu kalau "<span class="p-bold">tulisan Ini adalah paragraf 1</span>" dan "<span class="p-bold">Ini adalah paragraf 2</span>" itu adalah paragraf, yang tahu hanyalah kita yang sebagai manusia.</p>	

	<h4 class="sub-heading">Cara ngasih tahu browsernya bagaimana ?</h4>
	<p class="p-content">Cara ngasih tahu browsernya adalah dengan memasukan content tersebut atau isinya kedalam tag HTML. <br>
	Misalkan saya ingin menampilkan contentnya sebagai link atau istilahnya hyperlink, caranya bagaimana ? <br>
	Caranya dengan memasukan content tersebut kedalam tag <span class="p-bold">&lt;a&gt;</span> <br>
	Begitu pula paragraf, caranya sama dengan memasukan contentnya kedalam tag <span class="p-bold">&lt;p&gt;</span> <br>
	</p>

	<h4 class="sub-heading">Terus kita tahu tag-tagnya dari mana ?</h4>
	<p class="p-content">Apabila teman-teman ingin tahu daftar tag-tag pada HTML, teman-teman bisa kunjungi tutorial kami sampai selesai, karena untuk daftar tag-tag pada HTML sendiri, kami telah bagi menjadi beberapa bagian.</p>

	<p class="p-content">Teman-teman juga harus tahu, di HTML itu tidak akan mempedulikan yang namanya enter, ataupun spasi. <br>Seperti contoh diatas saja, padahalkan kita menuliskannya dibawah tetapi tetap pada saat ditampilkan di browser maka tampilnnya disamping, begitu juga dengan spasi atau tab. Contoh :</p>

	<div class="contoh-dokumen">
		<p>
			<span class="properti">
				&lt;!DOCTYPE
				html&gt;<br>
				&lt;html
				lang="en"&gt;<br>
				&lt;head&gt;<br>
				&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
				&nbsp;&nbsp;&lt;title&gt;Belajar Paragraf&lt;/title&gt;<br>
				&lt;/head&gt;<br>
				&lt;body&gt;<br>
			</span>
			<span class="nbsp">Ini adalah paragraf 1</span>
			<span class="nbsp">Ini adalah paragraf 2</span><br>
			</span>
			<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
			</span>
		</p>
	</div>

	<p class="p-content">Pada contoh diatas kita memberikan spasi setelah tulisan <span class="pbold">Ini adalah paragraf 1</span></p>

	<p class="p-content">Tetapi pada saat dijalankan, yang akan tampil :</p>
	<div class="contoh-dokumen">
		Ini adalah paragraf 1 Ini adalah paragraf 2
	</div>

	<p class="p-content">Hasilnya sama seperti contoh pertama. <br>Untuk memberikan spasi pada HTML, itu kita perlu yang namanya entitas, di HTML itu ada entitas yang namanya <span class="p-bold">&</span><span class="p-bold">nbsp</span> <br>
		<span class="p-bold">&</span><span class="p-bold">nbsp</span> sendiri digunakan untuk menambahkan spasi setelah kata apa. <br>
	Misalkan :</p>

	<div class="contoh-dokumen">
		<p>
			<span class="properti">
				&lt;!DOCTYPE
				html&gt;<br>
				&lt;html
				lang="en"&gt;<br>
				&lt;head&gt;<br>
				&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
				&nbsp;&nbsp;&lt;title&gt;Belajar Paragraf&lt;/title&gt;<br>
				&lt;/head&gt;<br>
				&lt;body&gt;<br>
			</span>
			<span class="nbsp">Ini adalah paragraf 1 &</span><span>nbsp; Ini adalah paragraf 2</span><br>
			</span>
			<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
			</span>
		</p>
	</div>

	<p class="p-content">Maka outputnya menjadi :</p>
	<div class="contoh-dokumen">
		Ini adalah paragraf 1 <span class="nbsp"></span>Ini adalah paragraf 2
	</div>
	
	<p class="p-content">Dengan menggunakan <span class="p-bold">&</span><span class="p-bold">nbsp</span> kita akan menambahkan spasi setelah kata tertentu.</p>


	<br>
	<a href="index.php?html=komentar" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=heading" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
