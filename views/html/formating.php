<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Format pada HTML</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	<h4 class="sub-heading">Format teks pada HTML</h4>
	<p class="p-content">Pemformatan pada HTML adalah digunakan untuk mengatur penggunaan tulisan pada halaman web, pemformatannya bisa berupa (jenis tulisan, ukuran, warna, dll).</p>
	<h4 class="sub-heading">Contoh :</h4>
	<div class="contoh-dokumen">
		<h1>Ini adalah pemformatan pada HTML</h1>
		<b>Ini menggunakan b</b><br>
		<i>Ini menggunakan i</i><br>
		<sub>Ini menggunakan sub</sub>
		<sup>Ini menggunakan sup</sup>
	</div>

	<h4 class="sub-heading">Keterangan :</h4>
	<ul id="ul-content">
		<li><span class="p-bold">&lt;b&gt;</span> digunakan untuk membuat font tebal</li>
		<li><span class="p-bold">&lt;i&gt;</span> digunakan untuk membuat font miring / italic</li>
		<li><span class="p-bold">&lt;sub&gt;</span> Untuk membuat subscript</li>
		<li><span class="p-bold">&lt;sup&gt;</span> Untuk membuat superscript</li>
	</ul>

	<h4 class="sub-heading">Tag Lain :</h4>
	<ul id="ul-content">
		<li><span class="p-bold">&lt;small&gt;</span> untuk membuat ukuran teks / font menjadi lebih kecil</li>
		<li><span class="p-bold">&lt;mark&gt;</span> untuk menandai suatu teks</li>
		<li><span class="p-bold">&lt;del&gt;</span> untuk mencoret teks</li>
		<li><span class="p-bold">&lt;em&gt;</span> membuat font miring /  italic</li>
		<li><span class="p-bold">&lt;strong&gt;</span> membuat font tebal atau bold</li>
		<li><span class="p-bold">&lt;ins&gt;</span> menambahkan garis bawah / undeline</li>
	</ul>

	<h4 class="sub-heading">Contoh elemen HTML <span class="p-bold">&lt;b&gt;</span> dan <span class="p-bold">&lt;strong&gt;</span></h4>
	<p class="p-content">Tag <span class="p-bold">&lt;b&gt;</span> dan <span class="p-bold">&lt;strong&gt;</span> merupakan tag pada HTML yang hampir mirip funsinya yaitu membuat font tebal, tetapi kedua tag ini memiliki perbedaan tag <span class="p-bold">&lt;b&gt;</span> hanya digunakan untuk memebuat font tebal sedangkan <span class="p-bold">&lt;strong&gt;</span> itu berbeda, walaupu secara visual tampilannya sama, tetapi dengan menggunakan strong itu seolah-olah elemen kita itu memiliki makna,ama halnya dengan <span class="p-bold">&lt;i&gt;</span> dengan <span class="p-bold">&lt;em&gt;</span>.</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;b&gt;</span>Ini merupakan teks menggunakan tag b<span class="properti">&lt;/b&gt;&lt;br&gt;</span><br>
		<span class="properti">&lt;strong&gt;</span>Ini merupakan teks menggunakan tag strong<span class="properti">&lt;/strong&gt;</span>
	</div>

	<p class="p-content">Apabila ditampilkan di web browser maka hasilnya sama persis</p>
	<div class="contoh-dokumen">
		<b>Ini teks menggunakan tag b</b><br>
		<strong>Ini teks menggunakan tag strong</strong>
	</div>

	<h4 class="sub-heading">Contoh Elemen HTML <span class="p-bold">&lt;i&gt;</span> dan <span class="p-bold">&lt;em&gt;</span></h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;i&gt;</span>Ini merupakan teks menggunakan tag i<span class="properti">&lt;/i&gt;&lt;br&gt;</span><br>
		<span class="properti">&lt;em&gt;</span>Ini merupakan teks menggunakan tag em<span class="properti">&lt;/em&gt;</span>
	</div>

	<div class="p-content">Maka outputnya :</div>
	<div class="contoh-dokumen">
		<i>Ini menggunakan i</i><br>
		<em>Ini menggunakan em</em>
	</div>

	<h4 class="sub-heading">Contoh Elemen HTML <span class="p-bold">&lt;small&gt;, &lt;mark&gt;,</span> dan <span class="p-bold">del&gt;</span></h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;small&gt;</span>Menggunakan small<span class="properti">&lt;/small&gt;&lt;br&gt;</span><br>
		<span class="properti">&lt;mark&gt;</span>teks dengan mark<span class="properti">&lt;/mark&gt;&lt;br&gt;</span><br>
		<span class="properti">&lt;del&gt;</span>teks dengan del<span class="properti">&lt;/del&gt;&lt;br&gt;</span><br>
		<span class="properti">&lt;ins&gt;</span>teks dengan ins<span class="properti">&lt;/ins&gt;</span>
	</div>
	<p class="p-content">Maka outputnya :</p>
	<div class="contoh-dokumen">
		<small>Menggunakan small</small><br>
		<mark>teks dengan mark</mark><br>
		<del>teks dengan del</del><br>
		<ins>teks dengan ins</ins><br>
	</div>

	<h4 class="sub-heading">Contoh Elemen HTML <span class="p-bold">&lt;sup&gt;</span> dan <span class="p-bold">sub&gt;</span></h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;p&gt;</span>Ini paragraf dengan superscript <span class="properti">&lt;sup&gt;</span> atas <span class="properti">&lt;/sup&gt;&lt;br&gt;</span><br>
		<span class="properti">&lt;p&gt;</span>teks dengan sub<span class="properti"> &lt;sup&gt;</span> bawah <span class="properti">&lt;/sup&gt;&lt;br&gt;</span><br>
	</div>

	<p class="p-content">Maka outputnya :</p>
	<div class="contoh-dokumen">
		<p>Ini paragraf dengan superscript <sup>atas</sup></p>
		<p>teks dengan sub <sub>bawah</sub></p>
	</div>

	<p class="p-content">Tag-tag diatas beserta contohnya merupakan beberapa format yang biasa diberikan pada teks.</p>

	<br>
	<a href="index.php?html=heading" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=breakLine" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
