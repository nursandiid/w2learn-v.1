<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Dasar</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Contoh dokumen HTML:</h4>
	<div class="contoh-dokumen">
		<span>
			<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
			&nbsp;&nbsp;&lt;title&gt;</span>Belajar HTML<span class="properti">&lt;/title&gt;<br>
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>
		<span class="nbsp properti">&lt;h1&gt;</span>Ini adalah heading</span><span class="properti">&lt;/h1&gt;</span><br>
		<span class="nbsp properti">&lt;p&gt;</span>Ini adalah paragraf</span><span class="properti">&lt;/p&gt;</span><br>
		</span>
		<span class="properti">
		&lt;/body&gt;<br>
		&lt;/html&gt;<br>
		</span>
		</span>
	</div>
	<p class="p-content">Buat teman-teman yang belum belajar mengenai HTML jangan khawatir mengenai kode di atas, pada tutorial selanjutnya kita akan belajar mengenai elemen HTML.</p>

	<p class="p-content">Jadi dokumen diatas merupakan struktur dasar dari dokumen html, lebih tepatnya lagi HTML yang akan kita pelajari adalah mengenai HTML5.</p>

	<p class="p-content">Struktur HTML di atas merupakan struktur dasar dari HTML5.
	</p>
	<ul id="ul-content">
		<li>Ada <span class="p-bold">&lt;!DOCTYPE html&gt;</span>merupakan sebagian dari ciri-ciri halaman web yang menggunakan HTML5</li>
		<li><span class="p-bold">&lt;html lang="en"&gt;</span>merupakan tag pembuka dari html, digunakan untuk memulai dalam menulis kode HTML.</li>
		<li><span class="p-bold">&lt;head&gt;</span>untuk menuliskan kode dibagian head seperti link, title, meta, dll.</li>
		<li><span class="p-bold">&lt;body&gt;</span>semua kode HTML nantinya kita letakan atau simpan di bagian body.</li>
	</ul>

	<h4>Contoh Heading :</h4>
	<div class="contoh-dokumen">
		<span><h1>&lt;h1&gt;Ini Heading 1&lt;/h1&gt;</h1>
		<h2>&lt;h2&gt;Ini Heading 2&lt;/h2&gt;</h2>
		<h3>&lt;h3&gt;Ini Heading 3&lt;/h3&gt;</h3>
		<h4>&lt;h4&gt;Ini Heading 4&lt;/h4&gt;</h4>
		<h5 style="margin: 30px auto;">&lt;h5&gt;Ini Heading 5&lt;/h5&gt;</h5>
		<h6>&lt;h6&gt;Ini Heading 6&lt;/h6&gt;</h6></span>
	</div>
	<p class="p-content">
		Heading diatas merupakan heading dari yang terbesar sampai yang terkecil, h1 sendiri merupakan heading dengan ukuran yang paling besar, begitu pulan h2, h3 sampai h6, adalah heading pada HTML dengan ukuran yang berbeda pula.
	</p>

	<h4>Contoh Pragraf :</h4>
	<div class="contoh-dokumen">
		<span>
			<span class="properti">&lt;p&gt;</span>Ini Paragraf 1<span class="properti">&lt;/p&gt;</span><br>
			<span class="properti">&lt;p&gt;</span>Ini Paragraf 2<span class="properti">&lt;/p&gt;</span><br>
			<span class="properti">&lt;p&gt;</span>Ini Paragraf 3<span class="properti">&lt;/p&gt;</span><br>
		</span>
	</div>
	<p class="p-content">Begitu juga paragraf, untuk membuat paragraf sendiri kita cukup menggunakan tag &lt;p&gt;. Tutorial ini kita tidak akan menjelaskan banyak mengenai HTML, kita cuman berkenalan terlebih dahulu dengan beberapa elemen HTML diatas, pada tutorial-tutorial selanjutnya mungkin akan lebih jelas lagi mengenai elemen pada HTML.</p>

	<br>

	<a href="index.php?html=hello" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=elemen" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->