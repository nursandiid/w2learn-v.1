<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Div dan Span</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Div dan Span pada HTML</h4>
	<p class="p-content">Tag <span class="p-bold">&lt;div&gt;</span> dan <span class="p-bold">&lt;span&gt;</span> merupakan tag pada HTML yang cukup penting, atau istilah dari kedua tag ini iyalah block inline.  <br>
	Jadi kedua tag ini biasanya sering kita pakai untuk membungkus elemen HTML lain atau untuk menjadi lapisan pada halaman. <br>
	Pada tutorial kali ini kita tidak akan membahas detail kegunaan dari <span class="p-bold">&lt;div&gt;</span> dan <span class="p-bold">&lt;span&gt;</span> melainkan kami hanya akan memberikan beberapa contoh saja yang biasa dilakukan oleh <span class="p-bold">&lt;div&gt;</span> dan <span class="p-bold">&lt;span&gt;</span>.</p>

	<h4 class="sub-heading">Contoh penggunaan Div :</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;div style="width: 200px; height: 200px; backgroun-color: salmon; padding: 10px;"&gt;</span><br>
			<span class="properti nbsp">&lt;h4&gt;</span>
				Hallo Semua.
			<span class="properti">&lt;h4&gt;</span><br>
			<span class="properti nbsp">&lt;p&gt;</span>
				Selamat datang dihalaman web kami.
			<span class="properti">&lt;p&gt;</span><br>
		<span class="properti">&lt;/div&gt;</span><br>
	</div>

	<p class="p-content">Apabila kita jalankan nantinya akan ada kotak berykuran 200 x 200 yang warnanya pink dengan lebar padding 10px.</p>
	<div class="contoh-dokumen">
		<div style="background-color: salmon; width: 200px; height: 200px; padding: 10px;">
			<h4>Hallo Semua</h4>
			<p>Selamat datang dihalaman web kami.</p>
		</div>
	</div>

	<h4 class="sub-heading">Contoh Span :</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Pemrograman
			<span class="properti">&lt;span style="color: lightgreen;"&gt;</span>Web<span class="properti">&lt;span&gt;</span>
		<span class="properti">&lt;h4&gt;</span><br>
		<span class="properti">&lt;p&gt;</span>
			Selamat datang <span class="properti">&lt;span style="color: red; font-weight: bold;"&gt;</span>dihalaman web<span class="properti">&lt;span&gt;</span> kami.
		<span class="properti">&lt;p&gt;</span><br>
	</div>

	<p class="p-content">Walaupaun kita sedikit menggunakan sintak CSS dan kita memang belum belajar mengenai CSS, yang penting kita tahu dulu penggunaan <span class="p-bold">div</span> dan <span class="p-bold">span</span>. <br> Apabila kita jalankan kode diatas :</p>
	<div class="contoh-dokumen">
		<h1>Pemrograman <span style="color: lightgreen;">Web</span></h1>
		<p>Selamat datang <span style="color: red; font-weight: bold;">dihalaman web</span> kami.</p>
	</div>

	<p class="p-content">Apabila kita sudah mulai belajar CSS, nantinya kita akan sering sekali menggunakan tag <span class="p-bold">&lt;div&gt;</span> tersebut.</p>

	<br>
	<a href="index.php?html=tabelLanjut" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=entitas" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
