<!-- content -->
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Karakteristik HTML</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Tidak bersifat case sensitive</h4>

	<p class="p-content">Artinya apa ?.... <br> Artinya huruf besar maupun huruf kecil didalam penulisan kode HTML itu tidak akan mempengaruhi dalam segi layout ataupun tampilan. <br>Tidak Case Sensitive didalam penulisan HTML misalkan tag <span class="p-bold">&lt;h1&gt;</span> dengan menggunakan huruf kecil itu sama saja dengan <span class="p-bold">&lt;H1&gt;</span> dengan menggunakan huruf besar.</p>

	<h4 class="sub-heading">Ekstensi file berupa .htm atau .html</h4>

	<p class="p-content">Karakteristik kode HTML yang selanjutnya adalah ekstensi. <br> Ekstensi itu apa ?... <br>Pasti teman-teman sering menemukan yang namanya ekstensi, misalkan pada saat teman-teman menulis di Microsoft Word terus pada saat di save itu biasanya muncul misalkan <span class="p-bold">.doc</span> atau atau <span class="p-bold">.pdf</span> itu semua merupakan ekstensi. <br>begitu pula HTML, agar halaman web yang kita buat itu dapat muncul di web browser kita harus kasih ekstensi pada saat save kode HTML kita, biasanya yang paling sering itu <span class="p-bold">.htm</span> atau <span class="p-bold">.html</span> <span class="p-bold">.php</span><span class="p-bold"> .asp</span> dll.</p>
	
	<h4 class="sub-heading">Terdiri atas tag pembuka dan tag penutup dan ada juga tag HTML yang tidak memiliki penutup</h4>

	<p class="p-content">Inilah mengapa HTML dikatakan bahasa markup, karena bahasa HTML adalah sebuah bahasa yang diawali oleh tag pembuka dan diakhiri oleh tag penutup, walaupun ada beberapa tag HTML yang tidak memiki penutup seperti <span class="p-bold">&lt;br&gt; , &lt;hr&gt;</span> tetapi hampir semua tag HTML memiliki peutup. <br> Misalkan saya punya tag <span class="p-bold">&lt;h1&gt;</span> Isi tag html <span class="p-bold">&lt;/h1&gt;</span> <br> keterangan : <br>
	<ul id="ul-content">
		<li>Tag <span class="p-bold">&lt;h1&gt;</span> merupakan tag pembuka</li>
		<li>Sedangkan <span class="p-bold">&lt;/h1&gt;</span> merpakan tag penutup dan biasanya diikuti tanda <span class="p-bold">" / "</span> tetapi tag pembuka tidak.</li>
		<li>"Isi tag html" merupakan isi dari tag <span class="p-bold">h1</span> yang nantinya akan dicetak sebagai huruf besar dengan ukuran <span class="p-bold">h1</span></li>
	</ul>
	</p>

	<h4 class="sub-heading">Tag-tag saling berpasangan dan bersarang</h4>

	<p class="p-content">Seperti yang telah dijelaskan diatas, bahasa HTML merupakan sebuah bahasa yang terdiri dari serangkaian markup, tag-tag yang saling berpasangan dan besarang. </p>
	<br>

	<a href="index.php?html=sejarah" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=editor" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->