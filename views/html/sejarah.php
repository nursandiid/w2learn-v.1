<!-- content -->
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Sejarah HTML</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Sejarah HTML</h4>

	<p class="p-content">Jadi HTML itu pertama kali di ciptakan oleh yang namanya tim Berners-lee.</p>

	<p class="p-content">Tim Berners-lee ini adalah orang yang berjasa sampai saat ini kenapa kita bisa mengakses web tiap hari, tetapi selain dia menciptakan atau mengajukan spesifikasi HTML yang pertama dia juga menemukan banyak hal, seperti HTTP, HTML, WWW, Web Browser pertama, Web Server pertama, dan halaman web pertama.</p> 

	<p class="p-content">Jadi berkat bapa Berners-lee lah kita bisa mengakses halaman web setiap hari, mencari berbagai sumber dari internet, kita juga sekarang bisa facebookan setiap hari.</p> 

	<p class="p-content">Selain dari pada itu, dia juga menjadi direktur dari sebuah organisasi atau consortium yang namanya W3C. Bapa Berners-lee juga yang betugas mebuat standar-standar untuk segala sesuatu yang ada di web.</p>

	<h4 class="sub-heading">Apa itu W3C ?...</h4>

	<p class="p-content">W3C merupakan kependekan dari World Wide Web Consortium adalah suatu konsorsium yang bekerja untuk mengembangkan standar-standar untuk World Wide Web (WWW). <br> Spesifikasi teknologi-teknologi utama yang dipakai sebagai basis utama web, seperti URL (Uniform Resource Locator), HTTP (Hypertext Transfer Protocol), dan HTML (HyperText Markup Language) dikembangkan dan diatur oleh badan ini. </p>

	<h4 class="sub-heading">Tahun berapa W3C dibuat ?....</h4>

	<p class="p-content">W3C dibuat pada 20 Oktober 1994 oleh Tim Berners-Lee, didirikan oleh Massachusetts Institue of Tekchnology (MIT). <br> W3C bekerja dengan komunitas global untuk membuat standard internasional client dan server yang memungkinkan perdagangan dan komunikasi online melalui internet. <br> W3C juga menghasilkan software acuan. W3C Netscape Communications Corporation adalah salah satu anggota pendiri. Konsorsium ini dijalankan oleh MIT LCS, INIRA Institute national the Recherce en Informatique sebuah lembaga peneletian ilmu komputer perancis, bekerja sama dengan CERN Consei Europpen pour le Recherce Nulcleaire, tampat lahirnya Web. <br> W3C didanai oleh industri yang menjadi angggotanya, tetapi produknya tersedia gratis. Direktur W3C adalah tim Berners-Lee yang menemukan world wide web di CERN.</p>

	<h4 class="sub-heading">Apa saja yang dilakukan W3C ?...</h4>

	<p class="p-content">Kegiatan utama yang dilakukan oleh W3C adalah untuk mengembangkan protokol dan panduan yang akan memastikan perkembangan dari web ini dalam jangka panjang.</p>

	<h4 class="sub-heading">Standar-standarnya apa saja ?...</h4>

	<p class="p-content">Standar-standarnya sendiri hampir mencakup semua, Ada Web Design dan Aplications, Arsitektur Web, Web Servis, Dll.</p>

	<p class="p-content">Web Design seperti HTML dan CSS. <br> Jadi misalkan HTML punya tang baru atau ada tag-tag lama yang hilang itu gara-gara W3C ini.</p>

	<h4 class="sub-heading">Lalu aturan itu di implementasikannya gimana ?...</h4>	
	
	<p class="p-content">Implementasikannya gini, misalkan W3C udah punya aturan baru,  maka mereka akan membuat aturan itu supaya bisa di implementasikan di Web Browser. <br> Itulah kenapa ?..... <br> Salah satu alasan kita harus selalu update Web Browsernya terus, karena supaya kita juga bisa mengupdate teknologi yang dirumuskan oleh W3C tadi.</p>		

	<p class="p-content">tetapi, karena yang membuat Web Browser itu berbeda, perusahaannya berbeda, developernya juga berbeda, maka tiap-tiap Web Browser tersebut mengimplementasikan aturan-aturan dari W3c tadi juga berbeda, walaupun perbedaanya tidak akan terlalu jauh.</p>

	<h4 class="sub-heading">Kenapa kita harus instal semua browser ?...</h4>

	<p class="p-content">Itulah kenapa kita harus instal semua browser, karena supaya kita bisa lihat perbedaanya baik dari segi tampilan, performance, dan sebagainya. <br> Jadi apabila HTML punya standar baru pasti setelah itu Web Browser juga update.</p>

	<h4 class="sub-heading">Sejarah versi HTML</h4>
	<p>
		<ul id="ul-content">
			<li>HTML 2.0 (RFC 1866) disetujui sebagai standar 22 September 1955</li>
			<li>HTML 3.2 pada tanggal 14 Januari 1955</li>
			<li>HTML 4.0 pada tanggal 18 Desember 1997</li>
			<li>HTML 4.1 (minor fixes) pada tanggal 24 Desember 1999</li>
			<li>ISO / IEC 15445:200 ("ISO HTML", berdasar pada HTML 4.01 Strict) pada tanggal 15 Mei 2000</li>
			<li>HTML 5 (stabil) rekomendasi W3C pada tanggal 28 Oktober 2014</li>
		</ul>
		Jadi itulah perkembangan HTML dari versi pertama (HTML 2.0) dan sampai yang sekarang yaitu HTML 5.
	</p> <br>


	<a href="index.php?html=pengenalan" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=karakteristik" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->