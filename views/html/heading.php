<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Heading</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4 class="sub-heading">Heading pada HTML</h4>
	<p class="p-content">Contoh Heading :</p>
	<div class="contoh-dokumen">
		<span><h1>&lt;h1&gt;Ini Heading 1&lt;/h1&gt;</h1>
		<h2>&lt;h2&gt;Ini Heading 2&lt;/h2&gt;</h2>
		<h3>&lt;h3&gt;Ini Heading 3&lt;/h3&gt;</h3>
		<h4>&lt;h4&gt;Ini Heading 4&lt;/h4&gt;</h4>
		<h5 style="margin: 30px auto;">&lt;h5&gt;Ini Heading 5&lt;/h5&gt;</h5>
		<h6>&lt;h6&gt;Ini Heading 6&lt;/h6&gt;</h6></span>
	</div>

	<h4 class="sub-heading">Ada berapa heading pada HTML ?</h4>
	<p class="p-content">Di HTML ada 6 heading yang berurutan dari yang terbesar hingga yang terkecil, yaitu : <br></p>
	<ul>
		<li><span class="p-bold">&lt;h1&gt;, &lt;h6&gt;, &lt;h3&gt;, &lt;h4&gt;, &lt;h5&gt;, &lt;h6&gt;</span></li>
	</ul>

	<p class="p-content"><span class="p-bold">&lt;h1&gt;</span> merupakan heading dengan ukuran font paling besar, sedangkan <span class="p-bold">&lt;h6&gt; </span>adalah heading dengan ukuran font paling kecil. <br>
	Heading akan sering digunakan apabila kita misalkan membuat majalah online, atikel, atau sebagainya yang didalam content tersebut terdapat tulisan besar itu semua merupakan heading, ada yang besar kita sebutnya dengan heading &lt;h1&gt;, sedangkan heading yang lebih kecil kita sebutnya sub-heading.</p>

	<h4 class="sub-heading">Contoh :</h4>
	<div class="contoh-dokumen">
		<p>
			<span class="properti">
				&lt;!DOCTYPE
				html&gt;<br>
				&lt;html
				lang="en"&gt;<br>
				&lt;head&gt;<br>
				&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
				&nbsp;&nbsp;&lt;title&gt;</span>Belajar Heading<span class="properti">&lt;/title&gt;<br>
				&lt;/head&gt;<br>
				&lt;body&gt;<br>
			</span>
			<span class="nbsp">
				<span class="properti">&lt;h1&gt;</span>Ini adalah heading 1<span class="properti">&lt;/h1&gt;</span><br> 

			</span>
			<span class="properti">
				&lt;/body&gt;<br>
				&lt;/html&gt;<br>
			</span>
		</p>
	</div>

	<p class="p-content">Maka outputnya :</p>
	<div class="contoh-dokumen">
		<h1>Ini adalah heading 1</h1>
	</div>

	<p class="p-content">Begitu juga dengan heading 2 - 6, cara penggunaanya sama dengan heading 1, cuman yang membedakan hanya ukuran font yang dihasilkan.</p>

	<h4 class="sub-heading">Misalkan ada contoh :</h4>
	<div class="contoh-dokumen">
		<p>
			<span class="properti">
				&lt;!DOCTYPE
				html&gt;<br>
				&lt;html
				lang="en"&gt;<br>
				&lt;head&gt;<br>
				&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
				&nbsp;&nbsp;&lt;title&gt;</span>Belajar Heading<span class="properti">&lt;/title&gt;<br>
				&lt;/head&gt;<br>
				&lt;body&gt;<br>
			</span>
			<span class="nbsp">
				<span class="properti">&lt;h1&gt;</span>3 Hal yang harus anda ketahui mengenai HTML<span class="properti">&lt;/h1&gt;</span><br>
			</span>
			<span class="properti" style="margin-left: 30px">&lt;ul&gt;</span><br>
			<span class="properti" style="margin-left: 60px;">&lt;li&gt;</span>Apa itu HTML ? <span class="properti">&lt;/li&gt;</span><br>
			<span class="properti" style="margin-left: 60px;">&lt;li&gt;</span>Sejarah HTML<span class="properti">&lt;/li&gt;</span><br>
			<span class="properti" style="margin-left: 60px;">&lt;li&gt;</span>Untuk apa HTML ? <span class="properti">&lt;/li&gt;</span><br>
			<span class="properti" style="margin-left: 30px">&lt;/ul&gt;</span><br>

			<span class="properti nbsp">&lt;h4&gt;</span>Apa itu HTML ? <span class="properti">&lt;/h4&gt;</span><br>
			<span class="properti nbsp">&lt;p&gt;</span>HTML merupakan kependekan dari Hypertext Markup Language adalah sebuah bahasa markup yang digunakan untuk membuat sebuah halaman web dan menampilkan berbagai informasi di dalam sebuah browser atau web browser.<span class="properti">&lt;/p&gt;</span></span><br>

			<span class="properti nbsp">&lt;h4&gt;</span>Sejarah HTML<span class="properti">&lt;/h4&gt;</span><br>
			<span class="properti nbsp">&lt;p&gt;</span>Jadi HTML itu pertama kali di ciptakan oleh yang namanya tim Berners-lee.....<span class="properti">&lt;/p&gt;</span></span><br>

			<span class="properti nbsp">&lt;h4&gt;</span>Untuk apa HTML ? <span class="properti">&lt;/h4&gt;</span><br>
			<span class="properti nbsp">&lt;p&gt;</span>HTML adalah sebuah bahasa yang diguanakan untuk membuat halaman web.....<span class="properti">&lt;/p&gt;</span></span><br>

			<span class="properti">
				&lt;/body&gt;<br>
				&lt;/html&gt;<br>
			</span>
		</p>
	</div>

	<p class="p-content">Misalkan ada halaman web seperti diatas, walaupun kita belum belajar mengenai list atau daftar tidak apa-apa ikutin aja terus, yang penting kita tahu dulu penggunaan heading. <br>
	pada kode diatas ada tag <span class="p-bold">&lt;h1&gt;</span> yang isinya 3 hal yang harus anda ketahui mengenai HTML, tag <span class="p-bold">&lt;h1&gt;</span> tersebut merupakan judul dari artikelnya, sedangkan <span class="p-bold">&lt;h4&gt;</span>merupakan sub heading dari artikelnya. <br>Apabila ditampilkan maka outputnya :</p>
	<div class="contoh-dokumen">
		<h1>3 hal yang harus anda ketahui mengenai HTML</h1>
		<ul>
			<li>Apa itu HTML ?</li>
			<li>Sejarah HTML</li>
			<li>Untuk apa HTML ?</li>
		</ul>
		<h4>Apa itu HTML ?</h4>
		<p>HTML merupakan kependekan dari Hypertext Markup Language adalah sebuah bahasa markup yang digunakan untuk membuat sebuah halaman web dan menampilkan berbagai informasi di dalam sebuah browser atau web browser.</p>
		<h4>Sejarah HTML</h4>
		<p>Jadi HTML itu pertama kali di ciptakan oleh yang namanya tim Berners-lee.....</p>
		<h4>Untuk apa HTML ?</h4>
		<p>HTML adalah sebuah bahasa yang diguanakan untuk membuat halaman web.....</p>
	</div>

	<p class="p-content">Beberapa contoh diatas merupakan beberapa contoh sederhana yang bisa dilakukan oleh heading, agar teman-teman lebih mahir lagi dengan elemen heading, teman-teman bisa kolaborasiin dengan pengetahuan yang sudah teman-teman dapat dari W2Learn. Sehingga akan membuat teman-teman lebih mahir lagi.</p>

	<br>
	<a href="index.php?html=paragraf" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=formating" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
