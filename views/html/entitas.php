<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Entitas</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Apa itu Entitas ?</h4>
	<p class="p-content">Entitas merupakan karakter khusus yang memiliki arti dalam kode html. <br> 
	Beberapa karakter memiliki arti khusus dalam HTML, seperti tanda lebih kecil (<) yang mendefinisikan awal dari sebuah tag HTML. Kalau kita ingin agar browser menampilkan tanda lebih kecil itu (<), kita harus menyisipkan entitas karakter dalam kode HTML kita.
	Entitas karakter memiliki 3 komponen: </p>
	
	<ul id="ul-content">
		<li>sebuah tanda dan (&).</li>
		<li>nama entitas atau sebuah # yang diikuti nomor entitas.</li>
		<li>dan diakhiri dengan tanda titik koma (;).</li>
	</ul>

	<h4 class="sub-heading">Beberapa entitas karakter yang sering dipakai :</h4>

	<table class="table table-hover">
		<tr>
			<th>Nama Entitas</th>
			<th>Nomor Entitas</th>
			<th>Keterangan</th>
			<th>Hasil</th>
		</tr>
		<tr>
			<td>&<span>copy;</span></td>
			<td>&#<span>169;</span></td>
			<td>Copyright</td>
			<td>&copy;</td>
		</tr>
		<tr>
			<td>&<span>reg;</span></td>
			<td>&#<span>174;</span></td>
			<td>Registered</td>
			<td>&reg;</td>
		</tr>
		<tr>
			<td>-</td>
			<td>&#<span>8482;</span></td>
			<td>Trademark</td>
			<td>&#8482;</td>
		</tr>
		<tr>
			<td>&<span>nbsp;</span></td>
			<td>&#<span>161;</span></td>
			<td>Non Break Space</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&<span>amp;</span></td>
			<td>&#<span>38;</span></td>
			<td>Ampersand</td>
			<td>&amp;</td>
		</tr>
		<tr>
			<td>&<span>laquo;</span></td>
			<td>&#<span>171;</span></td>
			<td>Angle Question Left</td>
			<td>&laquo;</td>
		</tr>
		<tr>
			<td>&<span>raquo;</span></td>
			<td>&#<span>187;</span></td>
			<td>Angle Question Right</td>
			<td>&raquo;</td>
		</tr>
		<tr>
			<td>&<span>quot;</span></td>
			<td>&#<span>34;</span></td>
			<td>Tanda Kutip Dua</td>
			<td>&quot;</td>
		</tr>
		<tr>
			<td>&<span>apos;</span></td>
			<td>-</td>
			<td>Tanda Kutip Satu</td>
			<td>&apos;</td>
		</tr>
		<tr>
			<td>&<span>lt;</span></td>
			<td>&#<span>60;</span></td>
			<td>Lebih Kecil</td>
			<td>&lt;</td>
		</tr>
		<tr>
			<td>&<span>gt;</span></td>
			<td>&#<span>61;</span></td>
			<td>Lebih Besar</td>
			<td>&gt;</td>
		</tr>
		<tr>
			<td>&<span>times;</span></td>
			<td>&#<span>215;</span></td>
			<td>Tanda Kali</td>
			<td>&times;</td>
		</tr>
		<tr>
			<td>&<span>divide;</span></td>
			<td>&#<span>247;</span></td>
			<td>Tanda Bagi</td>
			<td>&divide;</td>
		</tr>
	</table>

	<p class="p-content">Karakter entitas diatas merupakan beberapa yang dimiliki oleh HTML, mungkin masih ada beberapa yang belum saya sebutkan.</p>

	<br>
	<a href="index.php?html=divSpan" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=tag" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
