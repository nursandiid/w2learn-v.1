<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Atribut Input</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Atribut pada elemen input</h4>	
	<p class="p-content">Pada tutorial sebelumnya kit sudah membahas sebagian dari atribut yang dimiliki oleh elemen input, tutorial ini kita masih akan membahas mengenai atribut yang dimiliki oleh elemen HTML input.</p>
	<h4 class="sub-heading">4. Size</h4>
	<p class="p-content">Size digunakan untuk mengatur size atau panjang dari elemen input. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tanpa menggunakan name<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text"&gt;</span><br><br>

		<span class="properti">&lt;h4&gt;</span>Dengan name<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" size="50"&gt;</span><br><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Tanpa menggunakan size</h4>
		Nama : <input type="text"> <br><br>
		<h4>Dengan size</h4>
		Nama : <input type="text" size="50"> <br><br>
	</div>
	<h4 class="sub-heading">5. Max-length</h4>
	<p class="p-content">Max-length digunakan untuk membaatasi inputan dari user itu seberapa panjang. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tanpa menggunakan max-length<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text"&gt;</span><br><br>

		<span class="properti">&lt;h4&gt;</span>Dengan max-length<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" maxlength="8"&gt;</span><br><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Tanpa menggunakan max-length</h4>
		Nama : <input type="text"> <br><br>
		<h4>Dengan max-length</h4>
		Nama : <input type="text" maxlength="8"> <br><br>
	</div>
	<p class="p-content">Untuk memastikannya teman-teman bisa copy kode diatas.</p>
	<h4 class="sub-heading">6. Disabled</h4>
	<p class="p-content">Disabled digunakan untuk menonaktifkan elemen input. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tanpa menggunakan disabled<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text"&gt;</span><br><br>

		<span class="properti">&lt;h4&gt;</span>Dengan disabled<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" disabled&gt;</span><br><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Tanpa menggunakan disabled</h4>
		Nama : <input type="text"> <br><br>
		<h4>Dengan disabled</h4>
		Nama : <input type="text" disabled> <br><br>
	</div>
	<h4 class="sub-heading">7. Readonly</h4>
	<p class="p-content">Atribut readonly itu akan memberitahukan kalau elemen input tersebut hanya bisa dibaca saja bukan dirubah-rubah. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tanpa menggunakan readonly<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" value=""&gt;</span><br><br>

		<span class="properti">&lt;h4&gt;</span>Dengan readonly<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" value="example" readonly&gt;</span><br><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Tanpa menggunakan readonly</h4>
		Nama : <input type="text" value=""> <br><br>
		<h4>Dengan readonly</h4>
		Nama : <input type="text" value="example" readonly> <br><br>
	</div>

	<p class="p-content">Atribut diatas merupakan beberapa atribut yang dimiliki oleh elemen input, masih banyak sekali atribut yang dimiliki oleh elemen input terutama dari daftar atribut baru pada HTML5, untuk sekali lagi kita tidak akan membahas mengenai HTML5 baik itu tag maupun atribut, karena nantinya akan ada tutorial sendiri mengenai HTML5.</p>

	<br>
	<a href="index.php?htmlform=typeInput" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?htmlform=atributInput" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->

