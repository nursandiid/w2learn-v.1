<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Elemen Form</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Elemen HTML pada form</h4>
	<p class="p-content">Setelah sebelumnya kita mengetahui mengenai form atau tag form, tutorial ini kita akan membahas elemen-elemen apa saja yang bisa kita masukan kedalam form.</p>

	<h4 class="sub-heading">Beberapa elemen HTML pada form.</h4>
	<h4 class="sub-heading">1. Input</h4>
	<p class="p-content">Tag input adalah elemen HTML yang digunakan untuk menerima inputan dari user dalam bentuk textfield. <br> Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="text" name="nama"&gt;</span>
	</div>

	<input type="text" class="form-control">

	<h4 class="sub-heading">2. Select / List-box</h4>
	<p class="p-content"><span class="p-bold">select</span> sendiri berbeda dengan input, <span class="p-bold">select</span> akan menampilkan opsi / pilihan pada saat kita klik. <br>Didalan select juga kita perlu menambahkas elemen HTML yang namanya <span class="p-bold">option</span>, Option juga punya atribut yang namanya <span class="p-bold">value</span>. <br>Contoh :</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;select name="kelas"&gt;</span><br>
			<span class="properti nbsp">&lt;option&gt;</span>--- Jurusan ---<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="</span> Rekayasa Perangkat Lunak<span class="properti">"&gt;</span>Rekayasa Perangkat Lunak<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="</span> Teknik Komputer Jaringan<span class="properti">"&gt;</span>Teknik Komputer Jaringan<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="</span> Perbankan Syariah<span class="properti">"&gt;</span>Perbankan Syariah<span class="properti">&lt;/option&gt;</span><br>
		<span class="properti">&lt;/select&gt;</span>
	</div>

	<select name="" class="form-control">
		<option value="">--- Jurusan ---</option>
		<option value="Rekayasa Perangkat Lunak">Rekayasa Perangkat Lunak</option>
		<option value="Teknik Komputer Jaringan">Teknik Komputer Jaringan</option>
		<option value="Perbankan Syariah">Perbankan Syariah</option>
	</select>

	<h4 class="sub-heading">3. Textarea</h4>
	<p class="p-content"><span class="p-bold">Textarea</span> adalah elemen HTML yang hampir digunakan untuk meneriman inputan dari user, <span class="p-bold">Textarea</span> juga sangat mirip dengan tag <span class="p-bold">input</span> Cuman memiliki sedikit perbedaan. <br>Coba teman-teman perhatikan dengan seksama perbedaan input dengan textarea. <br>Contoh :</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;textarea rows="3" cols="5"&gt;</span>
		<span class="properti">&lt;/textarea&gt;</span>
	</div>

	<p class="p-content">Untuk melihat hasilnya silahkan teman-teman copykan kode diatas. <br><span class="p-bold">Textarea</span> juga memiliki atribut yang biasa digunakan : <br></p>
	<ul id="ul-content">
		<li>Rows untuk menambahkan rows atau baris (lebar / tinggi)</li>
		<li>Cols untuk menambahkan cols atau kolom (panjang)</li>
	</ul>
	<p class="p-content">Dengan menggunakan <span class="p-bold">Textarea</span> kita dapat menyimpan banyak teks atau kata, <span class="p-bold">Textarea</span> sendiri biasa digunakan untuk menuliskan pesan dan sebagainya.</p>
	<textarea cols="5" rows="3" class="form-control"></textarea>

	<h4 class="sub-heading">4. Button</h4>
	<p class="p-content">Untuk membuat tombol pada HTML itu kita bisa menggunakan tag HTML yang namanya <span class="p-bold">button</span>. <br>Button ini digunakan pada saat kita mengirimkan data, pada saat tombol buttonnya di tekan datanya terus dikirim. <br>Button juga memiliki bebrapa atribut diantaranya : type, name, dll.<br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;button type="</span>submit<span class="properti">" name="</span> submit"<span class="properti">&gt;</span>Simpan
		<span class="properti">&lt;/button&gt;</span>
	</div>

	<p class="p-content">Untuk melihat hasilnya silahkan copykan kode diatas.</p>
	<button class="btn btn-info btn-sm">Simpan</button>


	<br><br><br>
	<a href="index.php?htmlform=form" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?htmlform=typeInput" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
