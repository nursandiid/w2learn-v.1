<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Atribut Input</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Atribut pada elemen input</h4>
	<p class="p-content">Sebetulnya pada tutorial-tutorial sebelumnya kita sudah sering menggunakan atribut dari input ini, seperti value, type, dll. <br><br> Tutorial ini kita akan membahas mengenai atribut yang dimiliki oleh input, yang tentunya yang belum kita ketahui pada tutoria-tutorial sebelumnya.</p>
	<h4 class="sub-heading">1. Type</h4>
	<p class="p-content">Type merupakan atribut dari elemen input, yang digunakan untuk mendefinisikan mau type input apa yang kita gunakan, misal : text, password, checkbox, dll. <br>Untuk contoh dari type sendiri teman-teman bisa kunjungi tutorial sebelum ini.</p>
	<h4 class="sub-heading">2. Value</h4>
	<p class="p-content">Value merupakan atribut dari input yang kita gunakan agar elemen input tersebut terisi secara otomatis tan haru diisi oleh user. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tanpa menggunakan value<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text"&gt;</span><br><br>
		Kelas : <span class="properti">&lt;input type="text"&gt;</span><br><br>

		<span class="properti">&lt;h4&gt;</span>Dengan value<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" value="example"&gt;</span><br><br>
		Kelas : <span class="properti">&lt;input type="text" value="example"&gt;</span><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Tanpa menggunakan value</h4>
		Nama : <input type="text"> <br><br>
		Kelas : <input type="text"> <br>
		<h4>Dengan value</h4>
		Nama : <input type="text" value="example"> <br><br>
		Kelas : <input type="text" value="example"> <br>
	</div>
	<h4 class="sub-heading">3. Name</h4>
	<p class="p-content">Atribut name sendiri jarang kita gunakan sebelum nantinya kita bekerja dengan PHP, name sendiri akan menjadi key dari array asosiatif pada PHP. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tanpa menggunakan name<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text"&gt;</span><br><br>

		<span class="properti">&lt;h4&gt;</span>Dengan name<span class="properti">&lt;/h4&gt;</span> <br>
		Nama : <span class="properti">&lt;input type="text" value="example" name="nama"&gt;</span><br><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Tanpa menggunakan name</h4>
		Nama : <input type="text"> <br><br>
		<h4>Dengan name</h4>
		Nama : <input type="text" name="example"> <br><br>
	</div>
	<p class="p-content">Apabila teman-teman perhatikan mungkin tidak ada yang berbeda, tetapi itu mungkin secara visual, apabila kita sudah menggunakan bahasa PHP kita akan sangat perlu atribut name tadi. <br><br> Untuk atribut HTML lainya kita akan membahas pada tutorial selanjutnya.</p>

	<br>
	<a href="index.php?htmlform=typeInputLanjut" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?htmlform=atributInputLanjut" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
