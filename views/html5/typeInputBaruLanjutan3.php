<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Type input baru pada HTML5 Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type input baru pada HTML5 lanjutan</h4>
	<h4 class="sub-heading">6. type: datetime-local</h4>
 	<p class="p-content"><strong>&lt;tinput type ="datetime-local"&gt;</strong> digunakan untuk membuat kontrol input yang memungkinkan pengguna memasukkan tanggal dan waktu dengan mudah, termasuk tahun, bulan, dan hari serta waktu dalam jam dan menit. <br><br>Zona waktu lokal pengguna digunakan. UI kontrol bervariasi secara umum dari browser ke browser; Dukungan saat ini tidak merata, hanya dengan Chrome / Opera dan Edge di desktop dan sebagian besar versi modern peramban seluler yang memiliki penerapan yang dapat <digunakan class="br"></digunakan><br>Contoh :</p>
 	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="datetime-local"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="datetime-local">
	</div>
	<p class="p-content">type datetime-local hampir mirip-mirip dengan type date.</p>

 	<h4 class="sub-heading">7. type: time</h4>
 	<p class="p-content"><strong>&lt;input type ="time"&gt;</strong> jenis type ini membuat bidang masukan yang dirancang agar pengguna dapat dengan mudah memasukkan waktu (jam dan menit, dan opsional detik).

	Antarmuka pengguna kontrol akan bervariasi dari browser ke browser. Dukungannya bagus di browser modern, dengan Safari menjadi browser utama tunggal yang belum menerapkannya; di Safari, dan browser lain yang tidak mendukung &lt;time&gt;, itu akan menurun dengan anggun menjadi <strong>&lt;input type ="text"&gt;</strong> .</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="time"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="time">
	</div>
	<p class="p-content">Untuk lebih jelasnya teman-teman bisa coba sendiri.</p>
 
	<h4 class="sub-heading">8. type: month</h4>
	<p class="p-content"><strong>&lt;input type ="month"&gt;</strong> digunakan untuk membuat kolom masukan yang memungkinkan pengguna memasukkan bulan dan tahun yang memungkinkan bulan dan tahun mudah masuk. Nilai adalah string yang nilainya dalam format "YYYY-MM", di mana YYYY adalah tahun empat digit dan MM adalah nomor bulannya. <br><br>

	UI kontrol bervariasi secara umum dari browser ke browser; Dukungan saat ini tidak merata, hanya dengan Chrome / Opera dan Edge di desktop - dan versi browser seluler paling modern - memiliki implementasi yang dapat digunakan.<br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="month"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="month">
	</div>

	<h4 class="sub-heading">9. type: week</h4>
	<p class="p-content"><strong>&lt;input type ="week"&gt;</strong> membuat kolom masukan yang memudahkan masuk setahun ditambah jumlah minggu selama tahun itu (mis. minggu 1 sampai 52).<br><br>

	Antarmuka pengguna kontrol bervariasi dari browser ke browser; Dukungan cross-browser saat ini agak terbatas, hanya dengan Chrome / Opera dan Microsoft Edge yang mendukungnya saat ini. <br><br>Di browser yang tidak mendukung, kontrol mendegradasi dengan anggun untuk berfungsi secara identik dengan <strong>&lt;input type ="text"&gt;</strong>.<br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="week"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="week">
	</div>

	<p class="p-content" style="float: right; font-size: 12px;"><strong>Referensi :</strong> <em><a href="https://developer.mozilla.org" target="_blank">https://developer.mozilla.org</a></em></p>
	<div class="clear"></div>
	<br>
	<a href="index.php?html5=typeInputBaruLanjutan2" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=typeInputBaruLanjutan4" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
