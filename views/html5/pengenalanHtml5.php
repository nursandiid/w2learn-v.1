<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Pengenalan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Pengenalan dengan HTML5</h4>
	<p class="p-content">Tutorial ini kita akan belajar mengenai HTML5, sebetulnya pada tutorial-tutorial sebelumnya kita sudah belajar mengenai HTML, tetapi HTML yang kita pakai merupakan HTML versi lama, kebetulan untuk saat ini HTML versi paling baru adalah HTML5, oleh karena itu kita akan belajar mengenai HTML5. <br><br></p> 
	<h4 class="sub-heading">Apa itu HTML5 ?</h4>
	<p class="p-content">Menurut wikipedia “HTML5 adalah sebuah bahasa markah untuk menstrukturkan dan menampilkan isi dari Waring Wera Wanua, sebuah teknologi inti dari Internet.  <br><br></p>

	<h4 class="sub-heading">Kenapa ada angka 5 nya ?</h4>
	<p class="p-content">Karena HTML5 adalah revisi kelima dari HTML (yang pertama kali diciptakan pada tahun 1990 dan versi keempatnya, HTML4, pada tahun 1997) dan hingga bulan Juni 2011 masih dalam pengembangan. <br><br>Tujuan utama pengembangan HTML5 adalah untuk memperbaiki teknologi HTMl agar mendukung teknologi multimedia terbaru, mudah dibaca oleh manusia dan juga mudah dimengerti oleh mesin. <br><br></p>
	
	<p class="p-content">HTML5 merupakan salah satu karya W3C (World Wide Web Consortium) untuk mendefinisikan sebuah bahasa markah tunggal yang dapat ditulis dengan cara HTML ataupun XHTML. <br><br>HTML5 merupakan jawaban atas pengembangan HTML 4.01 dan XHTML 1.1 yang selama ini berjalan terpisah, dan diimplementasikan secara berbeda-beda oleh banyak perangkat lunak pembuat web. <br><br>
	HTML5 juga meruapakan suatu spesifikasi atau standard yang dikeluarkan oleh W3C (World Wide Web Consortium) sebagai revisi dari standard HTML. <br><br>Saat ini ditulis kita banyak menjumpai aplikasi web yang masih menggunakan standard HTML4 namun beberapa sudah mulai migrasi ke standard web html5. <br><br>
	</p>

	<p class="p-content">Tutorial kami akan membahas mengenai bebrapa teknologi baru pada HTML5.</p>

	<br>
	<a href="" class="btn btn-default btn-prev disabled">Sebelumnya</a>
	<a href="index.php?html5=pendukung" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
