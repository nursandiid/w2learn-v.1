<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Type input baru pada HTML5 Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type input baru pada HTML5 lanjutan</h4>
	<h4 class="sub-heading">11. type: hidden</h4>
	<p class="p-content"><strong>&lt;input&gt; type="hidden"</strong> digunakan untuk menyembunyikan elemen dari input.
 	<br><br>Contoh :</p>
 	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="hidden"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="hidden">
	</div>
	<p class="p-content">Elemenya sebetulnya ada, tetapi disembunyikan.</p>
	<h4 class="sub-heading">12. type: color</h4>
	<p class="p-content"><strong>&lt;input&gt; type="color"</strong> digunakan untuk memberi elemen antarmuka pengguna yang memungkinkan pengguna menentukan warna, baik dengan menggunakan antarmuka pemetik warna visual atau dengan memasukkan warna ke dalam bidang teks dalam format heksadesimal <strong>#fff</strong>. Hanya warna sederhana (tanpa saluran alfa) yang diizinkan. <br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="color"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="color">
	</div>
	<p class="p-content">Dengan type color ini teman-teman bisa memilih warna yang disukai teman-teman.</p>

	<h4 class="sub-heading">13. type: tel</h4>
	<p class="p-content"><strong>&lt;input type ="tel"&gt;</strong> digunakan untuk membiarkan pengguna memasukkan dan mengedit nomor telepon.<br><br> Tidak seperti <strong>&lt;input type ="email"&gt;</strong> dan <strong>&lt;input type ="url"&gt;</strong>, nilai input tidak secara otomatis divalidasi ke format tertentu sebelum formulir dapat dikirim, karena format untuk nomor telepon sangat bervariasi di seluruh dunia. <br><br>

	Terlepas dari kenyataan bahwa input dari tipe tel secara fungsional identik dengan input teks standar, namun penggunaannya bermanfaat; yang paling cepat terlihat dari ini adalah bahwa browser seluler - terutama di ponsel - mungkin memilih untuk menampilkan keypad kustom yang dioptimalkan untuk memasukkan nomor telepon. <br><br>Menggunakan jenis masukan khusus untuk nomor telepon juga membuat penambahan validasi dan penanganan nomor telepon menjadi lebih nyaman. <br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="tel"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="tel" value="+62">
	</div>
	<p class="p-content">Hasilnya sama persis dengan type text.</p>

	<h4 class="sub-heading">14. type: range</h4>
	<p class="p-content"><strong>&lt;input&gt; type ="range"</strong> membiarkan pengguna menentukan nilai numerik yang harus tidak kurang dari nilai yang diberikan, dan tidak lebih dari nilai yang diberikan lainnya. <br><br>Nilai yang tepat, bagaimanapun, tidak dianggap penting. Ini biasanya diwakili dengan menggunakan slider atau kontrol dial daripada kotak entri teks seperti jenis input "nomor". <br><br>Karena jenis widget ini tidak tepat, seharusnya tidak digunakan kecuali nilai sebenarnya dari kontrol tidak penting.<br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="range"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="range">
	</div>
	<p class="p-content">type range hampir mirip dengan slider. <br>Itu diantaranya beberapa type dari input yang baru ada pada HTML5.</p>

	<p class="p-content" style="float: right; font-size: 12px;"><strong>Referensi :</strong> <em><a href="https://developer.mozilla.org" target="_blank">https://developer.mozilla.org</a></em></p>
	<div class="clear"></div>
	<br>
	<a href="index.php?html5=typeInputBaruLanjutan3" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=atributInputBaru" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
