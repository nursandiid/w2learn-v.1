<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Type input baru pada HTML5 Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type input baru pada HTML5 lanjutan</h4>

	<h4 class="sub-heading">4. type: date</h4>
	<p class="p-content"><strong>&lt;input type ="date"&gt;</strong> jenis input ini digunakan untuk membuat kolom masukan yang memungkinkan pengguna memasukkan tanggal, baik menggunakan kotak teks yang secara otomatis memvalidasi konten, atau menggunakan antarmuka pemetik tanggal khusus. <br><br>Nilai yang dihasilkan termasuk tahun, bulan, dan hari, tapi tidak waktunya. Waktu dan waktu-jenis masukan lokal mendukung waktu dan masukan tanggal / waktu.
 	<br><br>Contoh :</p>
 	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="date"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="date">
	</div>
	<p class="p-content">type date apabila text fieldnya diklik maka akan muncul kotak yang menampilkan format tanggal.</p>

 	<h4 class="sub-heading">5. type: datetime</h4>
 	<p class="p-content"><strong>&lt;input type ="datetime"&gt;</strong> adalah kontrol untuk memasukkan tanggal dan waktu (jam, menit, detik, dan sepersekian detik) serta zona waktu.<br><br> Fitur ini telah dihapus dari HTML WHATWG, dan tidak lagi didukung di browser. <br><br>Contoh :</p>
 	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="datetime"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="datetime">
	</div>
	<p class="p-content">Untuk datetime sendiri sudah mulai tidak disupport oleh browser-browser modern, saya sarankan untuk menggunakan datetime-local.</p>

	<p class="p-content" style="float: right; font-size: 12px;"><strong>Referensi :</strong> <em><a href="https://developer.mozilla.org" target="_blank">https://developer.mozilla.org</a></em></p>
	<div class="clear"></div>
	<br>
	<a href="index.php?html5=typeInputBaruLanjutan1" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=typeInputBaruLanjutan3" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
