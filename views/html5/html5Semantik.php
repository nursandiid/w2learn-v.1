<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Semantik</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Apa itu semantik ?</h4>
	<p class="p-content">Semantic secara tidak langsung menyatakan arti sebuah subjek seperti kata-kata atau kalimat-kalimat. Studi tersebut mencakup seberapa penting arti kata-kata tersebut dapat dipahami oleh manusia.</p>

	<h4 class="sub-heading">Apa itu semantik elemen pada HTML5 ?</h4>
	<p class="p-content">Di dalam HTML5 kita mengenal istilah yang namanya <em>semantic elements</em> atau elemen-elemen yang memiliki arti semantik.</p>

	<h4 class="sub-heading">Maksudnya apa ?</h4>
	<p class="p-content">Misalkan ada contoh :</p>
	<p class="p-content">Pada saat kita menggunakan HTML versi lama kita menuliskan seperti ini :</p>
	<img src="public/img/html4.png"><br>
	<span style="font-size: 11px;">Sumber: Google.com</span> <br><br>

	<p class="p-content">Dengan HTML5 kita rubah gaya penulisan lama menjadi :</p>
	<img src="public/img/html5.png"><br>
	<span style="font-size: 11px;">Sumber: Google.com</span> <br><br>

	<p class="p-content">Sekarang teman-teman bandingkan lebih mudah dimengerti mana antara HTML varsi lama dengan HTML5 ? <br>Pasti jawabanya HTML5. <br><br> Karena pada HTML versi lama untuk membuat sebuah navbar, section, article dan sebagainya itu kita perlu yang namanya <strong>&lt;div&gt;</strong> kemudian kita juga perlu menambahkan <strong>class</strong> agar bisa kita sleksi nanti. <br><br></p>

	<p class="p-content">Sekarang kita kembali lagi kepertanyaan sebelumnya, kenapa HTML5 dikatakan elemen smantik, yah karena tadi elemen-elemen pada HTML5 itu memiliki arti tersendiri dan lebih mudah untuk dipahami.</p>


	<br>
	<a href="index.php?html5=penggunaanAtributBaruLanjutan2" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="" class="btn btn-default btn-next disabled">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->