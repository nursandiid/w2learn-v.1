<!DOCTYPE html>
<HTML lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/HTML; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Tutorial HTML5</title>
	<link rel="icon" type="image/png" sizes="16x16" href="sandy2.png">
	<link rel="stylesheet" href="public/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="public/css/app.css">
	<link rel="stylesheet" href="public/css/style.css">
</head>
<body>
	<header class="header">
		<nav class="navbar navbar-default">
		 	<div class="container">
				<div class="row">
					<div class="navbar-header" style="padding-top: 10px; padding-left: 10px;">
						<a class="navbar-brand" href="">
							<img src="sandy.png" alt="logo" height="40">
						</a>
					</div>
					<div id="search" class="navbar-right">
						<form method="get" action="" autocomplete="off" class="search-box navbar-form hidden-xs">
							<input type="text" placeholder="Search..." style="background-color: #fff;">
							<i class="fa fa-search"></i>
						</form>
					</div>
				</div>
			</div>
		</nav>

		<nav class="navbar navbar-inverse navbar-custom " style="box-shadow: 0 0 30px rgba(0,0,0,.8);">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarm" aria-expanded="false" aria-controls="navbar">
			 				<span class="sr-only">Toggle navigation</span> 
			 				<span class="icon-bar"></span> 
			 				<span class="icon-bar"></span> 
			 				<span class="icon-bar"></span> 
			 			</button> 
		 			</div>
		 			<div class="menu-primary-menu-container">
						<div id="navbarm" class="row navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-left">
								<li class="active">
									<a href="">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
										HTML
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" style="height: 400px; overflow-y: auto;">
										<li><a href="index.php?html=pengenalan">Pengenalan HMTL</a></li> 
										<li><a href="index.php?html=sejarah">Sejarah HTML</a></li> 
										<li><a href="index.php?html=karakteristik">Karakteristik HTML</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html=editor">HTML Editor</a></li>
										<li><a href="index.php?html=hello">HTML Hello World</a></li>
										<li><a href="index.php?html=dasar">HTML Dasar</a></li>
										<li><a href="index.php?html=elemen">HTML Elemen</a></li>
										<li><a href="index.php?html=atribut">HTML Atribut</a></li> 
										<li><a href="index.php?html=komentar">HTML Komentar</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html=paragraf">HTML Paragraf</a></li> 
										<li><a href="index.php?html=heading">HTML Heading</a></li>
										<li><a href="index.php?html=formating">HTML Formating</a></li> 
										<li><a href="index.php?html=breakLine">HTML Break Line</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html=javascript">HTML JavaScript</a></li> 
										<li><a href="index.php?html=warna">HTML Warna</a></li> 
										<li><a href="index.php?html=css">HTML CSS</a></li> 
										<li><a href="index.php?html=link">HTML Link</a></li> 
										<li><a href="index.php?html=gambar">HTML Gambar</a></li> 
										<li><a href="index.php?html=daftar">HTML Daftar</a></li> 
										<li class="divider"></li>
										<li><a href="index.php?html=daftarLanjut">HTML daftar lanjut</a></li>
										<li><a href="index.php?html=tabel">HTML Tabel</a></li>
										<li><a href="index.php?html=tabelLanjut">HTML Tabel Lanjut</a></li> 
										<li><a href="index.php?html=divSpan">HTML Div dan Span</a></li>
										<li><a href="index.php?html=entitas">HTML Entitas</a></li> 
										<li><a href="index.php?html=tag">HTML Daftar Tag</a></li> 
										<li class="divider"></li>
										<li><a href="index.php?html=tagLanjut">HTML Daftar Tag Lanjutan</a></li>
										<li><a href="index.php?html=daftarAtribut">HTML Daftar Atribut</a></li>
									</ul>
								</li> 

								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
										HTML Form
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="index.php?htmlform=form">HTML Form</a></li> 
										<li><a href="index.php?htmlform=elemenForm">HTML Elemen Form</a></li> 
										<li class="divider"></li>
										<li><a href="index.php?htmlform=typeInput">HTML Type Input</a></li>
										<li><a href="index.php?htmlform=typeInputLanjut">HTML Type Input Lanjut</a></li> 
										<li><a href="index.php?htmlform=atributInput">HTML Atribut Input</a></li>
										<li class="divider"></li>
										<li><a href="index.php?htmlform=atributInputLanjut">HTML Atribut Input Lanjut</a></li>
									</ul>
								</li> 

								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
										HTML5
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu" style="height: 400px; overflow-y: auto;">
										<li><a href="index.php?html5=pengenalan">Pengenalan HTML5</a></li>
										<li><a href="index.php?html5=pendukung">Pendukung HTML5</a></li>
										<li><a href="index.php?html5=struktur">Struktur HTML5</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html5=keunggulan">Keunggulan HTML5</a></li>
										<li><a href="index.php?html5=halBaru">Hal baru HTML5</a></li>
										<li><a href="index.php?html5=elemenBaru">Elemen baru HTML5 dan fungsinya</a></li>
										<li><a href="index.php?html5=typeInputBaru">Type input baru pada HTML5 dan fungsinya</a></li>
										<li><a href="index.php?html5=typeInputBaruLanjutan1">Type input baru pada HTML5 lanjutan 1</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html5=typeInputBaruLanjutan2">Type input baru pada HTML5 lanjutan 2</a></li>
										<li><a href="index.php?html5=typeInputBaruLanjutan3">Type input baru pada HTML5 lanjutan 3</a></li>
										<li><a href="index.php?html5=typeInputBaruLanjutan4">Type input baru pada HTML5 lanjutan 4</a></li>
										<li><a href="index.php?html5=atributInputBaru">Atribut input baru HTML5 dan fungsinya</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html5=penggunaanAtributBaru">HTML5 penggunaan atribut baru pada input</a></li>
										<li><a href="index.php?html5=penggunaanAtributBaruLanjutan1">HTML5 penggunaan atribut baru pada input Lanjutan 1</a></li>
										<li><a href="index.php?html5=penggunaanAtributBaruLanjutan2">HTML5 penggunaan atribut baru pada input Lanjutan 2</a></li>
										<li class="divider"></li>
										<li><a href="index.php?html5=semantik">HTML5 semantik</a></li>
									</ul>
								</li> 

								<!-- <li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
										HTML Graphis
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="">HTML canvas</a></li>
										<li><a href="">HTML SVG</a></li>
										<li class="divider"></li>
										<li><a href="">HTML google maps</a></li>
									</ul>
								</li> 

								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
										HTML Media
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a href="">HTML media</a></li>
										<li><a href="">HTML video</a></li>
										<li><a href="">HTML audio</a></li>
										<li class="divider"></li>
										<li><a href="">HTML youtube</a></li>
									</ul>
								</li> -->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>
	
	<div class="container-fluid excerpt text-center php" id="judul">
	<?php if( isset($_GET["html"]) ) { ?>
		<h1>Tutorial HTML</h1>
	<?php } elseif( isset($_GET["htmlform"]) ) { ?>
		<h1>Tutorial HTML Form</h1>
	<?php } elseif( isset($_GET["html5"]) ){ ?>
		<h1>Tutorial HTML5</h1>
	<?php } else { ?>
		<h1>Tutorial HTML</h1>
	<?php } ?>
	</div>

	<!-- content -->
	<section>
		<div class="container" id="main" style="margin-top: 20px;"> 
			<div class="row">
				<button type="button" id="switchM0" class="visible-sm visible-xs">
					<i class="caret"></i>
				</button>

				<!-- sidebar kiri-->
			<div class="col-md-2 hidden-sm hidden-xs" id="nav1" > 

			<!-- html -->
			<?php if( isset( $_GET["html"] ) ){ ?>
				<h3>Pengenalan HTML</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html=pengenalan">Pengenalan HMTL</a></li> 
					<li><a href="index.php?html=sejarah">Sejarah HTML</a></li> 
					<li><a href="index.php?html=karakteristik">Karakteristik HTML</a></li> 
				</ul>

				<h3>HTML Editor</h3>
				<ul class="menubar boxbg">
					<li><a href="index.php?html=editor">HTML Editor</a></li>
					<li><a href="index.php?html=hello">HTML Hello World</a></li>
				</ul>

				<h3>HTML Dasar</h3>
				<ul class="menubar boxbg">
					<li><a href="index.php?html=dasar">HTML Dasar</a></li>
					<li><a href="index.php?html=elemen">HTML Elemen</a></li>
					<li><a href="index.php?html=atribut">HTML Atribut</a></li> 
					<li><a href="index.php?html=komentar">HTML Komentar</a></li>
					<li><a href="index.php?html=paragraf">HTML Paragraf</a></li> 
					<li><a href="index.php?html=heading">HTML Heading</a></li>
					<li><a href="index.php?html=formating">HTML Formating</a></li> 
					<li><a href="index.php?html=breakLine">HTML Break Line</a></li>
				</ul>
				<h3>HTML Script</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html=javascript">HTML JavaScript</a></li> 
				</ul> 

				<h3>HTML Lanjut</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html=warna">HTML Warna</a></li> 
					<li><a href="index.php?html=css">HTML CSS</a></li> 
					<li><a href="index.php?html=link">HTML Link</a></li> 
					<li><a href="index.php?html=gambar">HTML Gambar</a></li> 
					<li><a href="index.php?html=daftar">HTML Daftar</a></li> 
					<li><a href="index.php?html=daftarLanjut">HTML daftar lanjut</a></li>
					<li><a href="index.php?html=tabel">HTML Tabel</a></li>
					<li><a href="index.php?html=tabelLanjut">HTML Tabel Lanjut</a></li> 
					<li><a href="index.php?html=divSpan">HTML Div dan Span</a></li>
					<li><a href="index.php?html=entitas">HTML Entitas</a></li> 
					<li><a href="index.php?html=tag">HTML Daftar Tag</a></li> 
					<li><a href="index.php?html=tagLanjut">HTML Daftar Tag Lanjutan</a></li>
					<li><a href="index.php?html=daftarAtribut">HTML Daftar Atribut</a></li>
				</ul> 

			<?php } elseif( isset( $_GET["htmlform"] ) ){ ?>
			<!-- html form -->
				<h3>HTML Form</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?htmlform=form">HTML Form</a></li> 
					<li><a href="index.php?htmlform=elemenForm">HTML Elemen Form</a></li> 
					<li><a href="index.php?htmlform=typeInput">HTML Type Input</a></li>
					<li><a href="index.php?htmlform=typeInputLanjut">HTML Type Input Lanjut</a></li> 
					<li><a href="index.php?htmlform=atributInput">HTML Atribut Input</a></li>
					<li><a href="index.php?htmlform=atributInputLanjut">HTML Atribut Input Lanjut</a></li> 
				</ul>

			<?php } elseif( isset( $_GET["html5"] ) ){ ?>
			<!-- html5 -->
				<h3>HTML5 Pengenalan</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html5=pengenalan">Pengenalan HTML5</a></li>
					<li><a href="index.php?html5=pendukung">Pendukung HTML5</a></li>
					<li><a href="index.php?html5=struktur">Struktur HTML5</a></li>
					<li><a href="index.php?html5=keunggulan">Keunggulan HTML5</a></li>
				</ul>

				<h3>HTML5 Hal Baru</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html5=halBaru">Hal baru HTML5</a></li>
					<li><a href="index.php?html5=elemenBaru">Elemen baru HTML5 dan fungsinya</a></li>
					<li><a href="index.php?html5=typeInputBaru">Type input baru pada HTML5 dan fungsinya</a></li>
				</ul>
				<h3>HTML5 Lanjutan</h3> 
				<ul class="menubar boxbg">
					<li><a href="index.php?html5=typeInputBaruLanjutan1">Type input baru pada HTML5 lanjutan 1</a></li>
					<li><a href="index.php?html5=typeInputBaruLanjutan2">Type input baru pada HTML5 lanjutan 2</a></li>
					<li><a href="index.php?html5=typeInputBaruLanjutan3">Type input baru pada HTML5 lanjutan 3</a></li>
					<li><a href="index.php?html5=typeInputBaruLanjutan4">Type input baru pada HTML5 lanjutan 4</a></li>
				</ul>
				<h3>HTML5 Hal Baru Lanjutan</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html5=atributInputBaru">Atribut input baru HTML5 dan fungsinya</a></li>
					<li><a href="index.php?html5=penggunaanAtributBaru">HTML5 penggunaan atribut baru pada input</a></li>
					<li><a href="index.php?html5=penggunaanAtributBaruLanjutan1">HTML5 penggunaan atribut baru pada input Lanjutan 1</a></li>
					<li><a href="index.php?html5=penggunaanAtributBaruLanjutan2">HTML5 penggunaan atribut baru pada input Lanjutan 2</a></li>
				</ul>

				<h3>HTML5 Semantik</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html5=semantik">HTML5 semantik</a></li>
				</ul>
			<?php } else{ ?>
				<h3>Pengenalan HTML</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html=pengenalan">Pengenalan HMTL</a></li> 
					<li><a href="index.php?html=sejarah">Sejarah HTML</a></li> 
					<li><a href="index.php?html=karakteristik">Karakteristik HTML</a></li> 
				</ul>

				<h3>HTML Editor</h3>
				<ul class="menubar boxbg">
					<li><a href="index.php?html=editor">HTML Editor</a></li>
					<li><a href="index.php?html=hello">HTML Hello World</a></li>
				</ul>

				<h3>HTML Dasar</h3>
				<ul class="menubar boxbg">
					<li><a href="index.php?html=dasar">HTML Dasar</a></li>
					<li><a href="index.php?html=elemen">HTML Elemen</a></li>
					<li><a href="index.php?html=atribut">HTML Atribut</a></li> 
					<li><a href="index.php?html=komentar">HTML Komentar</a></li>
					<li><a href="index.php?html=paragraf">HTML Paragraf</a></li> 
					<li><a href="index.php?html=heading">HTML Heading</a></li>
					<li><a href="index.php?html=formating">HTML Formating</a></li> 
					<li><a href="index.php?html=breakLine">HTML Break Line</a></li>
				</ul>
				<h3>HTML Script</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html=javascript">HTML JavaScript</a></li> 
				</ul> 

				<h3>HTML Lanjut</h3> 
				<ul class="menubar boxbg"> 
					<li><a href="index.php?html=warna">HTML Warna</a></li> 
					<li><a href="index.php?html=css">HTML CSS</a></li> 
					<li><a href="index.php?html=link">HTML Link</a></li> 
					<li><a href="index.php?html=gambar">HTML Gambar</a></li> 
					<li><a href="index.php?html=daftar">HTML Daftar</a></li> 
					<li><a href="index.php?html=daftarLanjut">HTML daftar lanjut</a></li>
					<li><a href="index.php?html=tabel">HTML Tabel</a></li>
					<li><a href="index.php?html=tabelLanjut">HTML Tabel Lanjut</a></li> 
					<li><a href="index.php?html=divSpan">HTML Div dan Span</a></li>
					<li><a href="index.php?html=entitas">HTML Entitas</a></li> 
					<li><a href="index.php?html=tag">HTML Daftar Tag</a></li> 
					<li><a href="index.php?html=tagLanjut">HTML Daftar Tag Lanjutan</a></li>
					<li><a href="index.php?html=daftarAtribut">HTML Daftar Atribut</a></li>
				</ul> 
				<?php } ?>
			</div>
			<!-- akhir sidebar kiri -->

			<!-- content -->

			<div class="col-md-7 noPadding" id="nav3">  
			<?php  
			// html
				if( isset( $_GET["html"] ) ){
					if( $_GET["html"] == "home" ){
						require 'views/html/pengenalan.php';
					}
					if( $_GET["html"] == "pengenalan" ){
						require 'views/html/pengenalan.php';
					}
					if( $_GET["html"] == "sejarah" ){
						require 'views/html/sejarah.php';
					}
					if( $_GET["html"] == "karakteristik"){
						require 'views/html/karakteristik.php';
					}
					if( $_GET["html"] == "editor"){
						require 'views/html/editor.php';
					}
					if( $_GET["html"] == "hello"){
						require 'views/html/hello.php';
					}
					if( $_GET["html"] == "dasar" ){
						require 'views/html/dasar.php';
					}
					if( $_GET["html"] == "elemen" ){
						require 'views/html/elemen.php';
					}
					if( $_GET["html"] == "atribut"){
						require 'views/html/atribut.php';
					}
					if( $_GET["html"] == "komentar" ){
						require 'views/html/komentar.php';
					}
					if( $_GET["html"] == "paragraf" ){
						require 'views/html/paragraf.php';
					}
					if( $_GET["html"] == "heading" ){
						require 'views/html/heading.php';
					}
					if( $_GET["html"] == "formating" ){
						require 'views/html/formating.php';
					}
					if( $_GET["html"] == "breakLine" ){
						require 'views/html/breakLine.php';
					}
					if( $_GET["html"] == "javascript"){
						require 'views/html/javascript.php';
					}
					if( $_GET["html"] == "warna" ){
						require 'views/html/warna.php';
					}
					if( $_GET["html"] == "css"){
						require 'views/html/css.php';
					}
					if( $_GET["html"] == "link" ){
						require 'views/html/link.php';
					}
					if( $_GET["html"] == "gambar"){
						require 'views/html/gambar.php';
					}
					if( $_GET["html"] == "tabel"){
						require 'views/html/tabel.php';
					}
					if( $_GET["html"] == "tabelLanjut"){
						require 'views/html/tabelLanjut.php';
					}
					if( $_GET["html"] == "daftar" ){
						require 'views/html/daftar.php';
					}
					if( $_GET["html"] == "daftarLanjut" ){
						require 'views/html/daftarLanjut.php';
					}
					if( $_GET["html"] == "divSpan"){
						require 'views/html/divSpan.php';
					}
					if( $_GET["html"] == "entitas"){
						require 'views/html/entitas.php';
					}
					if( $_GET["html"] == "tag"){
						require 'views/html/tag.php';
					}
					if( $_GET["html"] == "tagLanjut"){
						require 'views/html/tagLanjut.php';
					}
					if( $_GET["html"] == "daftarAtribut"){
						require 'views/html/daftarAtribut.php';
					}
					if( $_GET["html"] == "" ){
						require 'views/html/pengenalan.php';
					}
				}

			// html form
				elseif( isset( $_GET["htmlform"] ) ){
					if( $_GET["htmlform"] == "form" ){
						require 'views/htmlform/form.php';
					}
					if( $_GET["htmlform"] == "elemenForm" ){
						require 'views/htmlform/elemenForm.php';
					}
					if( $_GET["htmlform"] == "typeInput" ){
						require 'views/htmlform/typeInput.php';
					}
					if( $_GET["htmlform"] == "typeInputLanjut" ){
						require 'views/htmlform/typeInputLanjut.php';
					}
					if( $_GET["htmlform"] == "atributInput" ){
						require 'views/htmlform/atributInput.php';
					}
					if( $_GET["htmlform"] == "atributInputLanjut" ){
						require 'views/htmlform/atributInputLanjut.php';
					}
					if( $_GET["htmlform"] == "" ){
						require 'views/htmlform/form.php';
					}
				}

			// html5
				elseif( isset( $_GET["html5"] ) ){
					if( $_GET["html5"] == "pengenalan" ){
						require 'views/html5/pengenalanHtml5.php';
					}
					if( $_GET["html5"] == "pendukung" ){
						require 'views/html5/pendukung.php';
					}
					if( $_GET["html5"] == "keunggulan" ){
						require 'views/html5/keunggulan.php';
					}
					if( $_GET["html5"] == "struktur" ){
						require 'views/html5/struktur.php';
					}
					if( $_GET["html5"] == "halBaru" ){
						require 'views/html5/halBaru.php';
					}
					if( $_GET["html5"] == "elemenBaru" ){
						require 'views/html5/elemenBaru.php';
					}
					if( $_GET["html5"] == "typeInputBaru" ){
						require 'views/html5/typeInputBaru.php';
					}if( $_GET["html5"] == "typeInputBaruLanjutan1" ){
						require 'views/html5/typeInputBaruLanjutan1.php';
					}
					if( $_GET["html5"] == "typeInputBaruLanjutan2" ){
						require 'views/html5/typeInputBaruLanjutan2.php';
					}
					if( $_GET["html5"] == "typeInputBaruLanjutan3" ){
						require 'views/html5/typeInputBaruLanjutan3.php';
					}
					if( $_GET["html5"] == "typeInputBaruLanjutan4" ){
						require 'views/html5/typeInputBaruLanjutan4.php';
					}
					if( $_GET["html5"] == "atributInputBaru" ){
						require 'views/html5/atributInputBaru.php';
					}
					if( $_GET["html5"] == "penggunaanAtributBaru" ){
						require 'views/html5/penggunaanAtributBaru.php';
					}
					if( $_GET["html5"] == "penggunaanAtributBaruLanjutan1" ){
						require 'views/html5/penggunaanAtributBaruLanjutan1.php';
					}
					if( $_GET["html5"] == "penggunaanAtributBaruLanjutan2" ){
						require 'views/html5/penggunaanAtributBaruLanjutan2.php';
					}
					if( $_GET["html5"] == "semantik" ){
						require 'views/html5/html5Semantik.php';
					}
					if( $_GET["html5"] == "" ){
						require 'views/html5/pengenalanHtml5.php';
					}
				} else{
					require 'views/html/pengenalan.php';
				}
			?>
			</div>

			<!-- akhir content -->


			<!-- sidebar kanan -->
			<div class="col-md-3 hidden-xs" id="nav2"> 
				<h3>Belajar Lagi</h3> 
				<div class="iklan">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<script>
					  (adsbygoogle = window.adsbygoogle || []).push({
					    google_ad_client: "ca-pub-8107644676307326",
					    enable_page_level_ads: true
					  });
					</script>
				</div> 
				<ul class="menubar boxbg Default"> 
						<li><a href="index.php?html=pengenalan">HTML (27)</a></li> 
						<li><a href="index.php?htmlform=form">HTML Form (6)</a></li> 
						<li><a href="index.php?html5=pengenalan">HTML5 (16)</a></li> 
						<!-- <li><a href="">HTML Graphis (3)</a></li> 
						<li><a href="">HTML Media(4)</a></li> -->
				</ul>
				<div class="iklan">
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<script>
					  (adsbygoogle = window.adsbygoogle || []).push({
					    google_ad_client: "ca-pub-8107644676307326",
					    enable_page_level_ads: true
					  });
					</script>
				</div> 
			</div>
			<!-- akhir sidebar kanan -->
			</div>
		</div>
		<div style="margin-bottom: 100px;"></div>
	</section>
	<!-- akhir content -->

	<footer style="background-color: #000; border-top: 1px solid #f9f9f9;">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<p class="text-center" style="color: #f0f0f0; letter-spacing: .5px; word-spacing: 3px;">
						© Copyright 2019 W2learn. All Rights Reserved
					</p>
				</div>
			</div>
		</div>
	</footer>

	<div class="overlay" style="position: fixed; top: 0; bottom: 0; let: 0; right: 0; background-color: rgba(0,0,0, .6); z-index: 11111;"></div>
	
	<span id="scrollup" style="display: block; background-color: #5a68a5;"><i>↑</i></span>
	<script src="public/js/jquery.min.js"></script>
	<script src="public/js/bootstrap.min.js"></script>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script src="public/js/app.js"></script>
	<script>
		$(function() {
			// ambil data url
		   var url = window.location.href; 
		   $('.boxbg li a').each(function() {
		      // Jika URL pada menu sama dengan url
		      if (this.href === url) {
		         // Tambahkan kelas current pada menu ini
		         $(this).addClass('current');
		      }
		   });
		});
	</script>
</body>
</HTML>